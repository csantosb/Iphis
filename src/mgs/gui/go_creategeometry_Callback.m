function go_creategeometry_Callback(handles)
%go_creategeometry_Callback - call the creation routine for current
%detector
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$


% check that a detector has been selected
if handles.detector(1)== '-'
    msgbox('Detector not selected', 'Please select a detector');
    return
end

datapath= [handles.datapath, handles.detector, filesep];

%-------------------------------------------------------------------------------
%- check existence of previous data files
%-------------------------------------------------------------------------------
if exist([datapath,'potential_mapping.mat'], 'file')||...
        exist([datapath,'efield.mat'], 'file')||...
        exist([datapath,'drift_velocities.mat'], 'file')||...
        exist([datapath,'drift_velocity_data.mat'], 'file')||...
        exist([datapath,'weighting_potential_cathode_pixel_1.mat'], 'file')||...
        exist([datapath,'weighting_field_cathode_pixel_1.mat'], 'file')||...
        exist([datapath,'weighting_potential_anode_pixel_1.mat'], 'file')||...
        exist([datapath,'weighting_field_anode_pixel_1.mat'], 'file'),

    ButtonName= questdlg('Delete data related to previous computation?', '', 'Yes','No','No');
    if strcmp(ButtonName, 'Yes')
        delete([datapath,'*.mat'])
    end
    update_Callback(handles)
end

%-------------------------------------------------------------------------------
%- call the creation routine
%-------------------------------------------------------------------------------
feval(handles.detector,handles);
update_Callback(handles); 

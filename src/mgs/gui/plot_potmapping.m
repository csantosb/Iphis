function plot_potmapping(handles)
%plot_potmapping - plot the potental mapping of the selected detector
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: january 2006$

% TODO: rewrite

%fprintf('[ATTENTION] This is the external mapping]\n');

% Loading of data
datapath= [handles.datapath, handles.detector, filesep];
location=[datapath,'geometry.mat'];
[filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
if filename==0,return,else,load([pathname,filename]),end
cathode=double(cathode);

location=[datapath,'potential_mapping.mat'];
[filename, pathname]=uigetfile(location, 'Select your potential mapping file ... ');
if filename==0,return,else,load([pathname,filename]),end
t= double(potential);

% figure n.1  Potential Mapping

figure,hold on
%set(gcf,'name',[handles.logo_str,handles.date_str])

hsurfaces=slice(t,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
set(hsurfaces,'FaceColor','interp','EdgeColor','none');
hcont=contourslice(t,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],...
    [voltage.cathode:(voltage.anode-voltage.cathode)/15:voltage.anode]);
set(hcont,'EdgeColor','interp','LineWidth',1)

% if handles.planar=='y'
%     p=patch(isosurface(cathode~=0,.9),'FaceColor','white','EdgeColor','none');
%     isonormals(cathode~=0,p);
%     isocolors(cathode~=0,p);
% end

view(3)
set(gca,'DataAspectRatio',[1 1 1]);
axis tight;
camlight ;
set(gcf,'Renderer','zbuffer');
lighting phong;
camproj orthographic
colorbar
grid minor
title('Potential Mapping')

hcont=contourslice((cathode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)
hcont=contourslice((anode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)
hcont=contourslice((capsule~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)



% figure n.2  Error
figure, whitebg(gcf,'white'),semilogy(vectError,'b'),grid,xlabel('number of loops'),ylabel('error')
%set(gcf,'name',[handles.logo_str,handles.date_str])


function update_Callback(handles)
%update_Callback - update buttons visibility on the detector panel
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

datapath= [handles.datapath, handles.detector, filesep];

if exist([datapath,'geometry.mat']),
    set(handles.plot_creategeometry,'Visible','on'),
%    set(handles.info,'Visible','on');
else
    set(handles.plot_creategeometry,'Visible','off'),
%    set(handles.info,'Visible','off')
end

if exist([datapath,'potential_mapping.mat']),
    set(handles.plot_potmapping,'Visible','on'),
else, 
    set(handles.plot_potmapping,'Visible','off'),
end

if exist([datapath,'efield.mat'])
    set(handles.plot_electricfield,'Visible','on'),
else, 
    set(handles.plot_electricfield,'Visible','off'),
end

if exist([datapath,'drift_velocities.mat'])  
    set(handles.plot_carriervelocity,'Visible','on'),
else, 
    set(handles.plot_carriervelocity,'Visible','off'),
end

if exist([datapath,'weightingpotential_cathode_pixel1.mat']) &...
        exist([datapath,'weightingpotential_anode_pixel1.mat'])
    set(handles.plot_weightingpotential,'Visible','on'),
else
    set(handles.plot_weightingpotential,'Visible','off'),
end

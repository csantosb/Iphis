function detectorlist_Callback(hObject, handles)
%detectorlist_CreateFcn
%       o hObject    handle to detectorlist (see GCBO)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: november 2005$

contents= get(hObject,'String');
detector= contents{get(hObject,'Value')};
datapath= handles.datapath;
handles.detector= detector;

if handles.detector(1)== '-'
    update_Callback(handles);
    guidata(hObject, handles); 
    return;
end

%-------------------------------------------------------------------------------
%- detectors properties
%-------------------------------------------------------------------------------
if (strcmp(detector,'planar')==1 | strcmp(detector,'smartpet')==1)
    handles.detectortype='planar';
    handles.scancoord= 'cartesian';
    set(handles.scancylindrical,'Value',0);
    set(handles.scancartesian,'Value',1);
else
    handles.detectortype='cylindrical';
    handles.scancoord= 'cylindrical';
    set(handles.scancylindrical,'Value',1);
    set(handles.scancartesian,'Value',0);
end

if strcmp(handles.scancoord, 'cartesian')
    set(handles.txtscanx, 'String', 'x');
    set(handles.txtscany, 'String', 'y');
else
    set(handles.txtscanx, 'String', 'r');
    set(handles.txtscany, 'String', 'theta');
end

%-------------------------------------------------------------------------------
%- create data folders if necessary
%-------------------------------------------------------------------------------
comment= ' ';
if ~(exist([datapath, detector],'dir')==7),
    eval(['!mkdir "', datapath, detector, '"'])
    if ~(exist([datapath, detector, filesep,'current'],'dir')==7),
        eval(['!mkdir "',datapath, detector, filesep, 'current"'])
    end
    if ~(exist([datapath, detector, filesep,'current'],'dir')==7),
        eval(['!mkdir "',datapath, detector, filesep, 'charge"'])
    end
    comment= 'data folders created';
end
update_Callback(handles);
guidata(hObject, handles); 

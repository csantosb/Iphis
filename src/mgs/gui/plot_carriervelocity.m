function plot_carriervelocity(handles)
%plot_carriervelocity - plot the electric field of the selected detector
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% TODO: rewrite


datapath= [handles.datapath, handles.detector, filesep];
% Loading of data
    location=[datapath,'geometry.mat'];
    [filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
    if filename==0,return,else,load([pathname,filename]),end
    
    location=[datapath,'drift_velocities.mat'];
    [filename, pathname]=uigetfile(location, 'Select your velocities file ... ');
    if filename==0,return,else,load([pathname,filename]),end
    
    % Just the values in germanium are of interest
    dve.x=double(dve.x).*(densge~=0);        dve.y=double(dve.y).*(densge~=0);        dve.z=double(dve.z).*(densge~=0); 
    dve.m=sqrt(dve.x.^2+dve.y.^2+dve.z.^2);
    dve.m=double(dve.m).*(densge~=0);
    dvh.x=double(dvh.x).*(densge~=0);        dvh.y=double(dvh.y).*(densge~=0);        dvh.z=double(dvh.z).*(densge~=0); 
    dvh.m= sqrt(dvh.x.^2+dvh.y.^2+dvh.z.^2);  dvh.m=double(dvh.m).*(densge~=0);
    %%%%%%%%%%%%%%%%%%%%




% Figure n. 1  Drift Velocity for electrons
    figure,hold on
%    set(gcf,'name',[handles.logo_str,handles.date_str])

    hsurfaces=slice(dve.m.*(densge~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    hcont=contourslice(dve.m.*(densge~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hcont,'EdgeColor','interp','LineWidth',1)

    reduc=5;
    x=vide.left+1:reduc:vide.left+detector.length;
    y=vide.up+1:reduc:vide.up+detector.height;
    z=vide.forward+1:reduc:vide.forward+detector.depth;
    [u,v,w]=meshgrid(x,y,z);

    Vx=dve.x(y,x,z);
    Vy=dve.y(y,x,z);
    Vz=dve.z(y,x,z);
    
    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    axis tight;
    colorbar
    alpha(1);
    view(3);
    set(gca,'DataAspectRatio',[1 1 1]);
    camlight;
    lighting phong;
%     if get(handles.CmMm,'Value')==0,        % mm
%         title('Drift Velocity for electrons in mm/s')
%     else,                                   % cm
        title('Drift Velocity for electrons in cm/s')
%     end




% Figure n. 2  Drift Velocity for holes  
    figure,hold on
%    set(gcf,'name',[handles.logo_str,handles.date_str])

    hsurfaces=slice(dvh.m.*(densge~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    hcont=contourslice(dvh.m.*(densge~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hcont,'EdgeColor','interp','LineWidth',1)

    Vx=dvh.x(y,x,z);
    Vy=dvh.y(y,x,z);
    Vz=dvh.z(y,x,z);
    
    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    axis tight;
    colorbar
    view(3)
    set(gca,'DataAspectRatio',[1 1 1]);
    camlight
    lighting phong
%     if get(handles.CmMm,'Value')==0,        % mm
%         title('Drift Velocity for holes in mm/s')
%     else,                                   % cm
        title('Drift Velocity for holes in cm/s')
%     end
    alpha(1)

disp('plotting of drift velocities done.')

   
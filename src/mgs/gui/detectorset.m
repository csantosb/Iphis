function detectorset(hObject, handles, parameter)
%detectorset -  set parameters for detector computation
%       o handles   structure with handles and user data (see GUIDATA)
%       o parameter name of the parameter to set

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

switch (parameter)
    case 'precision'
        if (get(hObject,'Value') == get(hObject,'Max'))
            handles.precision= 'single';
            fprintf('[Computation settings] Precision set to single\n');
        else
            handles.precision='double';
            fprintf('[Computation settings] Precision set to double\n');
        end
    otherwise
        fprintf('[WARNING] Parameter (%s) not implemented \n', parameter);
        return;
end

guidata(hObject, handles);        

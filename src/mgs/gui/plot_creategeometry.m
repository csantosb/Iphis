function plot_creategeometry(handles)
%plot_creategeometry - plot the geometry of the selected detector
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

%-------------------------------------------------------------------------------
%- load data
%-------------------------------------------------------------------------------
datapath= [handles.datapath, handles.detector, filesep, 'geometry.mat'];
[filename, pathname]= uigetfile(datapath, 'Select geometry file ... ');
if filename==0, return, else, load([pathname,filename]), end
cathode= single(cathode);
anode= single(anode);

%-------------------------------------------------------------------------------
%- plot geometry
%-------------------------------------------------------------------------------
answer= questdlg('Plot segments?', '', ' Yes',' No ','Exit',' No ');
if strcmp(answer,'Exit'), return, end

figure,hold on
set(gcf,'name',[handles.version,handles.releasedate])
view(3)
set(gca,'DataAspectRatio',[1 1 1]);
axis tight;
camlight left;
set(gcf,'Renderer','zbuffer');
lighting phong;
box

if strcmp(answer, ' Yes')
    for i=1:max(cathode(:))
        if i==1
            p=patch(isosurface(cathode==i,.5),'FaceColor','yellow','EdgeColor','none');
        else
            p=patch(isosurface(cathode==i,.5),'FaceColor','blue','EdgeColor','none');
        end
        pause(.5)
    end
    for i=1:max(anode(:))
        if i==1
            p=patch(isosurface(anode==i,.5),'FaceColor','yellow','EdgeColor','none');
        else
            p=patch(isosurface(anode==i,.5),'FaceColor','red','EdgeColor','none');
        end
        pause(.5)
    end
else,
    p=patch(isosurface(cathode~=0,.5),'FaceColor','blue','EdgeColor','none');
%     isonormals(cathode~=0,p);
%     isocolors(cathode~=0,p);
    p=patch(isosurface(anode~=0,.5),'FaceColor','red','EdgeColor','none');
%     isonormals(anode~=0,p);
%     isocolors(anode~=0,p);
end
text= {'cathode','anode'};
[a,d]= legend(gca,'cathode','anode');

p=patch(isosurface(isolant~=0,.5),'FaceColor','yellow','EdgeColor','none');
if sum(isolant(:))~=0,text=[text,{'passivation'}]; [a,d]=legend(gca,text);end

p=patch(isosurface(capsule~=0,.5),'FaceColor','green','EdgeColor','none');
if sum(capsule(:))~=0,text=[text,{'grounded contact'}]; [a,d]=legend(gca,text);end

p=patch(isosurface(electrode~=0,.5),'FaceColor','cyan','EdgeColor','none');
if sum(electrode(:))~=0,text=[text,{'electrodes'}]; [a,d]=legend(gca,text);end

title({handles.detector})
view(3)
set(gca,'DataAspectRatio',[1 1 1]);
axis tight;
camlight left;
set(gcf,'Renderer','zbuffer');
lighting phong;

%-------------------------------------------------------------------------------
%- plot density of impurities
%-------------------------------------------------------------------------------
figure,hold on
set(gcf,'name',[handles.version,handles.releasedate])

y=[1:dimension.nrows ];
x=[1:dimension.nwcols];
z=[1:dimension.nprofs];

xmin = min(x(:));
ymin = min(y(:));
zmin = min(z(:));
xmax = max(x(:));
ymax = max(y(:));
zmax = max(z(:));

hslice = surf(linspace(xmin,xmax,100),linspace(ymin,ymax,100),dimension.nprofs/2*ones(100));
rotate(hslice,[-1,0,0],-45)
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)
h = slice(densge,xd,yd,zd);
set(h,'FaceColor','interp','EdgeColor','none','DiffuseStrength',.8),hold on
hc=contourslice(densge,xd,yd,zd,linspace(impurities.low,impurities.high,10));
set(hc,'EdgeColor','k','LineWidth',0.5)



hold on

hs=slice(densge,[],[dimension.nrows/2],[dimension.nprofs/2]);
set(hs,'FaceColor','interp','EdgeColor','none');
hc=contourslice(densge,[],[dimension.nrows/2],[dimension.nprofs/2],linspace(impurities.low,impurities.high,10));
set(hc,'EdgeColor','k','LineWidth',0.5)

set(gca,'DataAspectRatio',[1 1 1]);
axis tight
box on
view(3)
camproj perspective
% lightangle(-45,45)
colormap (jet(24))
set(gcf,'Renderer','zbuffer')
% camlight;
% lighting phong;
% alpha(1)
grid minor
box

%
colorbar
title('Density of Impurities in mm-3')

%-------------------------------------------------------------------------------
%- plot charge
%-------------------------------------------------------------------------------
figure
set(gcf,'name',[handles.version,handles.releasedate])

% Electric constant of Ge and of Vide
ege = 16.2; %15.8 segun Marthina
eIso= 4;
e0 = 8.854187817e-16 * detector.step; % F / (1/10 mm) * detector.step


%%%%%%% Charge of the electron
e=1.60217733e-19;   % Coulomb.
matrice_affichage=(e/e0)*(densge/ege+double(isolant)/eIso);
hsurfaces=slice(matrice_affichage,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
set(hsurfaces,'FaceColor','interp','EdgeColor','none');

axis tight;
camproj perspective
view(3);
set(gca,'DataAspectRatio',[1 1 1]);
camlight;
lighting phong;
alpha(1)
grid minor
title('Charge')
box

colorbar
hcont=contourslice(matrice_affichage,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
hcont=contourslice(matrice_affichage,[1:dimension.nwcols/10:dimension.nwcols],[],[1:dimension.nprofs/10:dimension.nprofs]);
set(hcont,'EdgeColor','interp','LineWidth',1.5)

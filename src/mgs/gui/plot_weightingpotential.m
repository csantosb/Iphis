function plot_weightingpotential(handles)
% todo  rewrite all
datapath= [handles.datapath,handles.detector,filesep];
name= [handles.version,handles.releasedate];
load([datapath,'geometry.mat'])

cathode=single(cathode);
nbSegmentCathode= max(cathode(:));
anode=single(anode);
nbSegmentAnode= max(anode(:));
% 
% ButtonName= questdlg('Plot error?', '', ' Yes',' No ','Exit',' No ');
% if ButtonName =='Exit', return, end
% plotError= strcmp(ButtonName, 'Yes');

for i= 1:nbSegmentCathode,
    fprintf('[Cathode %i] Weighting potential\n', i);
    plot_potential(datapath, name, i, 'cathode', cathode, anode, capsule, dimension, 0);
end
for i= 1:nbSegmentAnode
    fprintf('[Anode %i] Weighting potential\n', i);
    plot_potential(datapath, name, i, 'anode', cathode, anode, capsule, dimension, 0);
end

fprintf('End\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot for one segment at a time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_potential(datapath, name, i, electrode, cathode, anode, capsule, dimension, plotError, isPlanar)
load([datapath, 'weightingpotential_', electrode, '_pixel', num2str(i), '.mat'], 'potential', 'vectError');

% figure n.1  Potential Mapping
figure,hold on
set(gcf,'name',name)
hsurfaces=slice(potential,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
set(hsurfaces,'FaceColor','interp','EdgeColor','none');
hcont=contourslice(potential,[],[],[dimension.nprofs/2],[1:-.1:.1,.1:-.01:.01]);
set(hcont,'EdgeColor','interp','LineWidth',1)

view(3)
set(gca,'DataAspectRatio',[1 1 1]);
axis tight;
camlight ;
set(gcf,'Renderer','zbuffer');
lighting phong;
camproj orthographic
colorbar
grid minor
title('Potential Mapping')

hcont=contourslice((cathode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)
hcont=contourslice((anode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)
hcont=contourslice((capsule~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
set(hcont,'EdgeColor','white','LineWidth',1)


function scanset(hObject, handles, parameter)
%detectorset -  set parameters for detector computation
%       o handles   structure with handles and user data (see GUIDATA)
%       o parameter name of the parameter to set

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

switch (parameter)
    case 'ascii'
        if isSelected(hObject)
           handles.scandata= 'ascii'; 
           set(handles.scanbinary, 'Value',0);
        else
           handles.scandata= 'binary'; 
           set(handles.scanbinary, 'Value',1);            
        end
    case 'binary'
        if isSelected(hObject)
           handles.scandata= 'binary'; 
           set(handles.scanascii, 'Value',0);
        else
           handles.scandata= 'ascii'; 
           set(handles.scanascii, 'Value',1);                   
        end        
    case 'cartesian'
        if isSelected(hObject)
           handles.scancoord= 'cartesian'; 
           set(handles.scancylindrical, 'Value',0);
           updateCoord(handles, 'cartesian');
        else
           handles.scancoord= 'cylindrical'; 
           set(handles.scancylindrical, 'Value',1);                   
           updateCoord(handles, 'cylindrical');
        end          
    case 'cylindrical'
        if isSelected(hObject)
           handles.scancoord= 'cylindrical'; 
           set(handles.scancartesian, 'Value',0);
           updateCoord(handles, 'cylindrical');
        else
           handles.scancoord= 'cartesian'; 
           set(handles.scancartesian, 'Value',1);                   
           updateCoord(handles, 'cartesian');
        end          
    case 'charge'
%         if isSelected(hObject)
%            handles.scanmode= 'charge'; 
%            set(handles.scancurrent, 'Value',0);
%         else
%            handles.scanmode= 'current'; 
%           set(handles.scancurrent, 'Value',1);                   
%         end          
    case 'current'
        if isSelected(hObject)
           handles.scanmode= 'current'; 
%           set(handles.scancharge, 'Value',0);
        else
           handles.scanmode= 'charge'; 
%           set(handles.scancharge, 'Value',1);                   
        end    
    case 'pack'
        contents= get(hObject,'String');
        handles.packsize= str2num(contents{get(hObject,'Value')});
    case 'x'
%        handles.scanx= str2double(get(hObject,'String'));
    case 'y'
%        handles.scany= str2double(get(hObject,'String'));
    case 'z'
%        handles.scanz= str2double(get(hObject,'String'));
    otherwise
        fprintf('[WARNING] Parameter (%s) not implemented \n', parameter);
        return;
end
guidata(hObject, handles) 

%===============================================================================
function bool= isSelected(h)
bool= (get(h,'Value') == get(h,'Max'));
    
%===============================================================================
function updateCoord(h, scancoord)
if strcmp(scancoord, 'cartesian')
    set(h.txtscanx,'String','x');    
    set(h.txtscany,'String','y');
    set(h.txtscanz,'String','z');
else
    set(h.txtscanx,'String','r');    
    set(h.txtscany,'String','theta');
    set(h.txtscanz,'String','z');
end

    
    
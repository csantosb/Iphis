function go_createefield_Callback(varargin)
%efield_Callback - compute the electric field for a given geometry and a
% potential mapping
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

% TODO: check scale + comment
%       why factor 2 on anodePotential (not in commented general solution)
%       generic name for anode and cathode

%-------------------------------------------------------------------------------
%- load and save data
%-------------------------------------------------------------------------------
location= [handles.datapath, handles.detector, filesep, 'geometry.mat'];
[filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
if filename==0,return,else,load([pathname,filename],'anode','cathode','densge','voltage','detector'),end

location= [handles.datapath, handles.detector, filesep,'potential_mapping.mat'];
[filename, pathname]=uigetfile(location, 'Select your potential_mapping file ... ');
if filename==0,return,else,load([pathname,filename],'potential'),end

location= [handles.datapath, handles.detector, filesep, 'efield.mat'];
[filename, pathname] = uiputfile(location, 'Select electric field data ... ');
if filename==0,return, end
    
%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
scale= 100; % cm (?)
dist= detector.step/scale; % distance between points
%-------------------------------------------------------------------------------
%- computation
%-------------------------------------------------------------------------------
% electric field everywhere V./cm
[efield_x, efield_y, efield_z]= gradient(-potential, dist);       

% electric field all around the Ge volume for the anode
elecPotential= (potential.*(densge~=0)) + voltage.anode*not(densge~=0);     
[efield_xa, efield_ya, efield_za]= gradient(-elecPotential, dist);
efield_x= 2*efield_xa.*(anode~=0) + efield_x.*not(anode~=0);  % why 2 ?
efield_y= 2*efield_ya.*(anode~=0) + efield_y.*not(anode~=0);
efield_z= 2*efield_za.*(anode~=0) + efield_z.*not(anode~=0);

%electric field all around the Ge volume for the cathode
elecPotential = (potential.*(densge~=0)) + voltage.cathode*not(densge~=0);      
[efield_xa, efield_ya, efield_za]= gradient(-elecPotential, dist);
efield_x= 2*efield_xa.*(cathode~=0) + efield_x.*not(cathode~=0);  % why 2 ?
efield_y= 2*efield_ya.*(cathode~=0) + efield_y.*not(cathode~=0);
efield_z= 2*efield_za.*(cathode~=0) + efield_z.*not(cathode~=0);

efield= sqrt((efield_x.*efield_x)+(efield_y.*efield_y)+(efield_z.*efield_z));

if max(efield)>1000
    warning('electric field higher than 1000 V/cm')
end

if handles.precision== 'single'
    efield_x= single(efield_x);
    efield_y= single(efield_y);
    efield_z= single(efield_z);
    efield= single(efield);
end

%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
save([pathname,filename],'efield_x','efield_y','efield_z','efield');

update_Callback(handles); 






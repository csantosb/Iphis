function plot_electric_field(handles)
%plot_electric_field - plot the electric field of the selected detector
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% TODO: rewrite

datapath= [handles.datapath, handles.detector, filesep];
% Loading of data
    location=[datapath,'geometry.mat'];
    [filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
    if filename==0,return,else,load([pathname,filename]),end
    
    location=[datapath,'potential_mapping.mat'];
    [filename, pathname]=uigetfile(location, 'Select your potential mapping file ... ');
    if filename==0,return,else,load([pathname,filename]),end
    
    location=[datapath,'efield.mat'];
    [filename, pathname]=uigetfile(location, 'Select your electric field file ... ');
    if filename==0,return,else,load([pathname,filename]),end
%%%%%%%%%%%%%%%%%%%%

% Just the values in germanium are of interest
    efield_x=double(efield_x).*(densge~=0);   % <-- For this, the contacts should belong to the germanium
    efield_y=double(efield_y).*(densge~=0);
    efield_z=double(efield_z).*(densge~=0);
    efield=sqrt(efield_x.^2+efield_y.^2+efield_z.^2);
    
% Figure n. 1 : Voltage mapping with Vectorial Electric Field
    figure,hold on
%    set(gcf,'name',[handles.logo_str,handles.date_str])
    hsurfaces=slice(potential,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    hcont=contourslice(potential,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],...
                                                        [voltage.cathode:(voltage.anode-voltage.cathode)/15:voltage.anode]);
    set(hcont,'EdgeColor','interp','LineWidth',1)

    reduc=7;
    x=vide.left+1:reduc:vide.left+detector.length;
    y=vide.up+1:reduc:vide.up+detector.height;
    z=vide.forward+1:reduc:vide.forward+detector.depth;
    [u,v,w]=meshgrid(x,y,z);

    Vx=efield_x(y,x,z);
    Vy=efield_y(y,x,z);
    Vz=efield_z(y,x,z);
    
    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    set(gcf,'Renderer','zbuffer');
    view(3);
    set(gca,'DataAspectRatio',[1 1 1]);
    camlight;
    lighting phong;
    alpha(1)
    colorbar
    axis tight;
    title('Voltage mapping with Vectorial Electric Field')

%     hcont=contourslice(t,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],-1:1);
%     set(hcont,'EdgeColor','white','LineWidth',1)
    hcont=contourslice((cathode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
    set(hcont,'EdgeColor','white','LineWidth',1)
    hcont=contourslice((anode~=0),[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],1);
    set(hcont,'EdgeColor','white','LineWidth',1)


% Figure n. 2 : Scalar and Vectorial Electric Field
    figure,hold on
%    set(gcf,'name',[handles.logo_str,handles.date_str])
    hsurfaces=slice(efield,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    hcont=contourslice(efield,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2]);
    set(hcont,'EdgeColor','interp','LineWidth',1)

    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    set(gcf,'Renderer','zbuffer');
    view(3);
    set(gca,'DataAspectRatio',[1 1 1]);
    camlight;
    lighting phong;
    alpha(1)
    colorbar
    colormap (jet(18))
%     if get(handles.CmMm,'Value')==0,        % mm
%         title('Electric field as seen by holes in V/mm')
%     else,                                   % cm
        title('Electric field as seen by holes in V/cm')
%     end
    axis tight;

    hcont=contourslice(efield,[dimension.nwcols/2],[dimension.nrows/2],[dimension.nprofs/2],-1:1);
    set(hcont,'EdgeColor','white','LineWidth',1)

    
    disp('plotting of electric field done.')
    
return
    
    
    % Figure n. 3 : 
    
    figure,hold on
%    set(gcf,'name',[handles.logo_str,handles.date_str])
    set(gca,'DataAspectRatio',[1 1 1]);

    p=patch(isosurface(efield,200),'FaceColor','blue','EdgeColor','none');
    isonormals(efield,p);
    isocolors(efield,p);
    [f verts] = reducepatch(isosurface(efield,200),0.07);
    h1 = coneplot(efield_x,efield_y,efield_z,verts(:,1),verts(:,2),verts(:,3),10);
    set(h1,'FaceColor','blue','EdgeColor','none');
    
%     [f verts] = reducepatch(isosurface(efield,150),0.07);
%     h1 = coneplot(efield_x,efield_y,efield_z,verts(:,1),verts(:,2),verts(:,3),10);
%     set(h1,'FaceColor','green','EdgeColor','none');
    
    p=patch(isosurface(efield,100),'FaceColor','red','EdgeColor','none');
    isonormals(efield,p);
    isocolors(efield,p);
    [f verts] = reducepatch(isosurface(efield,100),0.07);
    h1 = coneplot(efield_x,efield_y,efield_z,verts(:,1),verts(:,2),verts(:,3),12);
    set(h1,'FaceColor','red','EdgeColor','none');

    
%     xrange = linspace(1,dimension.nwcols,15);
%     yrange = linspace(1,dimension.nrows,15);
%     zrange = linspace(1,dimension.nprofs,15);
%     [cx,cy,cz] = meshgrid(xrange,yrange,zrange);
%     h2 = coneplot(efield_x,efield_y,efield_z,cx,cy,cz,12);
%     set(h2,'FaceColor','green','EdgeColor','none');
    
    view(3)
    set(gca,'DataAspectRatio',[1 1 1]);
%     axis tight;
    camlight left;
    set(gcf,'Renderer','zbuffer');
    lighting phong;
    camproj perspective
    box on
    grid minor

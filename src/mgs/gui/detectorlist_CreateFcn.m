function detectorlist_CreateFcn(hObject, eventdata, handles)
%detectorlist_CreateFcn
%       o hObject    handle to detectorlist (see GCBO)
%       o eventdata  reserved - to be defined in a future version of MATLAB
%       o handles    empty - handles not created until after all CreateFcns called

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: november 2005$

if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

handles.detectorlist= readDetectorList();


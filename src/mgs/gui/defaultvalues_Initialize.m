function defaultvalues_Initialize(handles, information)

set(handles.detectorlist,'String', readDetectorList());

% read default parameter file or last user file

% load default.mat

% check value of lastsaveddefault


% load lastsavedefault


% update handles structure

%check_out(handles);





%===============================================================================
function list= readDetectorList()
%readDetectorList
%   read the file mgs_detectorlist.txt containing all the detectors in use

separator= '-------';
filename= ['mgs', filesep, 'mgs_detectorlist.txt'];
fid= fopen(filename, 'rt');
if (fid==-1) fprintf('[Error] Can''t open file: %s\n', filename); 
    list= {'-------'};
    return;
end
list= textread(filename, '%s', 'commentstyle', 'c++');
% replace - by separator and remove empty lines
list(find(strcmp(list,'-')))= {separator};
fclose(fid);
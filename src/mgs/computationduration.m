function computationduration(description, duration)

if fix(duration/60)==0,
    fprintf('[%s] Computation duration: %ds\n', description, round(rem(duration,60)));
else, 
    fprintf('[%s] Computation duration: %dm%ds\n', description, fix(duration/60), ...
        round(rem(duration,60)));
end
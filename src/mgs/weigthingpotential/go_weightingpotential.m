function go_weightingpotential(handles)
%go_weightingpotential - compute the weighting potential for each segment
% (Laplace equation)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$


%-------------------------------------------------------------------------------
%- select computation precision
%-------------------------------------------------------------------------------
selectmethod = questdlg('Select accuracy of result','Weighting Potential','Fast','Best','help','Best');
if isempty(selectmethod) return, end
if strcmp(selectmethod, 'help')
    fprintf('Fast: 300 loops with SOR method (fastest convergence rate)\n');
    fprintf('Best: SOR then Relaxation, until convergence reached\n');
    fprintf('For more information, see documentation\n');
    return;
end
fprintf('Computing potential mapping\n');

%-------------------------------------------------------------------------------
%- load data
%-------------------------------------------------------------------------------
datapath= [handles.datapath, handles.detector, filesep];
location=[datapath,'geometry.mat'];
[filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
if filename==0,return,else,load([pathname,filename]),end

%-------------------------------------------------------------------------------
%- initialization
%-------------------------------------------------------------------------------
% number of segments (including the ring)
nbSegmentCathode= max(cathode(:));
nbSegmentAnode= max(anode(:));
% Source charge: zero for Laplace
q= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
% Filter for border conditions where the charge is fixed at 0.
filterOutOfDetector= not((cathode~=0)+(anode~=0)+(capsule~=0)+(package~=0)+(electrode~=0));
%pointOutsideDetector= find(densge==0);

%-------------------------------------------------------------------------------
%- compute weighting potential for each segment of the cathode
%-------------------------------------------------------------------------------
for iSegment= 1:nbSegmentCathode
    segmentBorder= (cathode==iSegment);   % Border conditions
    %-------------------------------------------------------------------------------
    %- starting value (use previously computed potential if possible)
    %-------------------------------------------------------------------------------
    [omega, curLoop, vectError, oldPotential, curError]= ...
        getStartingValues(datapath, dimension, iSegment, segmentBorder, 'cathode');
    %------------------------------------------------------------------------------- 
    %- compute weighting potential
    %-------------------------------------------------------------------------------
    tic
    switch(selectmethod)
    case 'Fast'
        [potential, curVectError, omega]= sor(oldPotential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];   
        fprintf('[Sor is finished (%d loops)]\n', length(curVectError));
    case 'Best'
        [potential, curVectError, omega]= sor(oldPotential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];
        fprintf('[Sor is finished (%d loops), continuing with relaxation]\n', ...
            length(curVectError));
        [potential, curVectError]= relaxation(potential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod);
        vectError= [vectError curVectError];       
        fprintf('[Relaxation is finished (%d loops)]\n', length(curVectError));
    otherwise
    end
    save([datapath,'weightingpotential_cathode_pixel', num2str(iSegment),'.mat'],...
        'potential','vectError','omega');
    outputElapsedTime('Cathode', iSegment, toc);
end %for iSegment=1:nbSegmentCathode

%-------------------------------------------------------------------------------
%- compute weighting potential for each segment of the anode
%-------------------------------------------------------------------------------
for  iSegment=1:nbSegmentAnode
    segmentBorder= (anode==iSegment);   % Border conditions
    %-------------------------------------------------------------------------------
    %- starting value (use previously computed potential if possible)
    %-------------------------------------------------------------------------------
    [omega, curLoop, vectError, oldPotential, curError]= ...
        getStartingValues(datapath, dimension, iSegment, segmentBorder, 'anode');
    %-------------------------------------------------------------------------------
    %- compute weighting potential
    %-------------------------------------------------------------------------------
    tic
    switch(selectmethod)
    case 'Fast'
        [potential, curVectError, omega]= sor(oldPotential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];   
        fprintf('[Sor is finished (%d loops)]\n', length(curVectError));
    case 'Best'
        [potential, curVectError, omega]= sor(oldPotential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];
        fprintf('[Sor is finished (%d loops), continuing with relaxation]\n', ...
            length(curVectError));
        [potential, curVectError]= relaxation(potential, segmentBorder, ...
            filterOutOfDetector, q, selectmethod);
        vectError= [vectError curVectError];       
        fprintf('[Relaxation is finished (%d loops)]\n', length(curVectError));
    otherwise
    end

    save([datapath,'weightingpotential_anode_pixel',num2str(iSegment),'.mat'],'potential','vectError','omega');
    computationduration(['Anode', num2str(iSegment)], toc);
end

update_Callback(handles);

%===============================================================================
function [omega, curLoop, vectError, oldPotential, curError]= ...
    getStartingValues(datapath, dimension, iSegment, segmentBorder, electrodeName)
% GETSTARTINGVALUES return the initialize data or data from a previously
% computed weighting potential
filename= ['weightingpotential_', electrodeName, '_pixel',num2str(iSegment),'.mat'];

omega=1;   
curError= 1;
curLoop= 1;  
oldPotential= double(segmentBorder); 
vectError= single([]);
if exist([datapath, filename]),
    load([datapath, filename],'potential','vectError','omega')
    if size(potential)== [dimension.nrows,dimension.nwcols,dimension.nprofs],
        oldPotential= potential;
        curError= vectError(end);
        curLoop= length(vectError);
    end
end


%===============================================================================
function outputElapsedTime(sElectrode, iSegment, iTimeElapsed)
% OUTPUTELAPSEDTIME output the duration of the computation for one segment
fprintf('[%s] Computed potential for segment %d ', sElectrode, iSegment);
if fix(iTimeElapsed/60)==0,
    fprintf('in %ds\n', round(rem(iTimeElapsed,60)));
else
    fprintf('in %dm %ds\n', fix(iTimeElapsed/60), round(rem(iTimeElapsed,60)));
end

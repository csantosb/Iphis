function go_potmapping(handles)
%go_potmapping - compute the potential mapping (Poisson equation)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

%-------------------------------------------------------------------------------
%- select computation precision
%-------------------------------------------------------------------------------
selectmethod = questdlg('Select accuracy of result','Potential mapping','Fast','Best','help','Best');
if isempty(selectmethod) return, end
if strcmp(selectmethod, 'help')
    fprintf('Fast: 300 loops with SOR method (fastest convergence rate)\n');
    fprintf('Best: SOR then Relaxation, until convergence reached\n');
    fprintf('For more information, see documentation\n');
    return;
end
fprintf('Computing potential mapping\n');

%-------------------------------------------------------------------------------
%- load and save data
%-------------------------------------------------------------------------------
datapath= [handles.datapath, handles.detector, filesep];
location=[datapath,'geometry.mat'];
[filename, pathname]=uigetfile(location, 'Select your geometry file ... ');
if filename==0,return,else,load([pathname,filename]),end
cathode= double(cathode);
anode= double(anode);

datapath= [handles.datapath, handles.detector, filesep];
location= [datapath, 'potential_mapping.mat'];
[filename, pathname]= uiputfile(location, 'Save potential mapping data ... ');
if filename==0, return, end

%-------------------------------------------------------------------------------
%- initialization
%-------------------------------------------------------------------------------
% constants
electronq= 1.60217733e-19;   % electron charge (coulomb)
epsilon0= 8.854187817e-12; % vacuum permittivity F/m
epsilon0= epsilon0*1e-3*(10/detector.step); % F/mm*grid
% relative permittivity of the detector material (previously called
% dielectric constant)
if isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'silicium')
    epsilonr= 11.9; % relative permitivity
elseif isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'CdTE')
    epsilonr= 10.9;
elseif (isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'germanium')) | ...
        ~isfield(impurities,'material')
    epsilonr= 16.2;     % (15.8 in Martina thesis)
else
    errordlg('Detector material not found in geometry file','Relative permittivity missing');
    return
end
epsilonisolant= 4;
if isa(isolant,'double')== 0
    isolant= double(isolant);
end
q= (electronq/epsilon0)*(densge/epsilonr + isolant/epsilonisolant); 

% frontier setup
voltFrontier= (anode~=0)*voltage.anode + (cathode~=0)*voltage.cathode + double(electrode);
if sum(densge(:))>0
    voltFrontier= voltFrontier + double(package~=0)*voltage.cathode;
end
notFrontier= double(not((cathode~=0)+(anode~=0)+(capsule~=0)+(package~=0)+(electrode~=0)));

%-------------------------------------------------------------------------------
%- load previous computation (if relevant)
%-------------------------------------------------------------------------------
omega=1;   
curError= 1;
curLoop= 1;  
oldPotential= anode; 
vectError= single([]);
if exist([pathname, filename]),
    load([pathname, filename],'potential','vectError','omega')
    if size(potential)== [dimension.nrows,dimension.nwcols,dimension.nprofs],
        oldPotential= potential;
        curError= vectError(end);
        curLoop= length(vectError);
    end
end

%-------------------------------------------------------------------------------
%- computation
%-------------------------------------------------------------------------------
tic
switch(selectmethod)
    case 'Fast'
        [potential, curVectError, omega]= sor(oldPotential, voltFrontier, notFrontier,...
            q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];   
        fprintf('[Sor is finished (%d loops)]\n', length(curVectError));
    case 'Best'
        [potential, curVectError, omega]= sor(oldPotential, voltFrontier, notFrontier,...
            q, selectmethod, curLoop, omega);
        vectError= [vectError curVectError];
        fprintf('[Sor is finished (%d loops), continuing with relaxation]\n', length(curVectError));
        [potential, curVectError]= relaxation(potential, voltFrontier, notFrontier,...
            q, selectmethod);
        vectError= [vectError curVectError];       
        fprintf('[Relaxation is finished (%d loops)]\n', length(curVectError));
    otherwise
end

fprintf('Computing potential mapping for external electrodes\n');

% EXPERIMENTAL
%   Solving for second tension matrix with special border conditions
%   __________________________________________________________________

% Border conditions
voltFrontier= (anode_ext~=0)*voltage.anode + (cathode_ext~=0)*voltage.cathode ...
    + double(electrode);% + double(package~=0)*voltage.cathode;
if sum(densge(:))>0,
    voltFrontier = voltFrontier + double(package~=0)*voltage.cathode;
end
% Filter for border conditions
notFrontier= double(not((cathode_ext~=0)+(anode_ext~=0)+(capsule~=0)+...
    (package~=0)+(electrode~=0)));

q= (electronq/epsilon0)*(cathode_ext/epsilonr + anode_ext/epsilonr + densge/epsilonr + isolant/epsilonisolant); 

omega_ext=1;   
curError_ext= 1;
curLoop= 1;  
vectError_ext= single([]);
oldPotential= (anode_ext~=0)*voltage.anode;

switch(selectmethod)
    case 'Fast'
        [potential_ext, curVectError_ext, omega_ext]= sor(oldPotential, voltFrontier, notFrontier,...
            q, selectmethod, curLoop, omega_ext);
        vectError_ext= [vectError_ext curVectError_ext];   
        fprintf('[Sor is finished (%d loops)]\n', length(curVectError_ext));
    case 'Best'
        [potential_ext, curVectError_ext, omega_ext]= sor(oldPotential, voltFrontier, notFrontier,...
            q, selectmethod, curLoop, omega_ext);
        vectError_ext= [vectError_ext curVectError_ext];
        fprintf('[Sor is finished (%d loops), continuing with relaxation]\n', length(curVectError));
        [potential_ext, curVectError_ext]= relaxation(potential_ext, voltFrontier, notFrontier,...
            q, selectmethod);
        vectError_ext= [vectError_ext curVectError_ext];       
        fprintf('[Relaxation is finished (%d loops)]\n', length(curVectError_ext));
    otherwise
end

% END OF EXPERIMENTAL


%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
save([pathname,filename],'potential','vectError','omega', ...
    'potential_ext','vectError_ext','omega_ext')
timeElapsed= toc; 
computationduration('Mapping potential', timeElapsed);

update_Callback(handles);


function [curPotential, vectError]= relaxation(oldPotential, voltFrontier, ...
    notFrontier, q, mode)
%relaxation - resolution of Poisson equation for a given geometry by
%   relaxation method (Jacobi)
% Inputs
%       o oldPotential  previously computed potential or initial potential
%       (ie anode) - 3D matrix
%       o voltFrontier  voltage at the frontier (anode, cathode, electrode
%       and package) - 3D matrix
%       o notFrontier   volume not belonging to a frontier (as above +
%       caspule) - 3D matrix
%       o q         relative charge in the whole volume - 3D matrix
%       o maxError  condition to end the computation
% Outputs
%       o oldPotential  last computed potential
%       o vectError     error vector for the computed loops
% See remarks at the end of file for an explanation on the method
% relaxation
% Best results but low convergence rate, see sor.m for a faster method

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.1 $Date: december 2005/february 2006$

% Revision 1.1: upgraded ending condition with a test on the error evolution

%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
curLoop= 1;
if strcmp(mode, 'Fast')
    maxLoop= 301;   % the computation will stop even if the error is huge
else 
    maxLoop= 5000;  % the computation will stop according to the error test
end
coeff= 1/6;   % coefficient (for a cubic 3D grid)
vectError= [];
[x y z]= size(oldPotential);
curPotential= oldPotential;
nbPoints= 1; %length(find(q~=0)); % includes detector(densge) and isolant
vectError= [];
if max(max(max(oldPotential)))== 1 % its Laplace equation for weighting field
    differentialError= 1e-12;      % when the difference between step n and n+1 is
else                             % below differentialError, stop computation
    differentialError= 1e-12;
end
continueError= 1;

%-------------------------------------------------------------------------------
%- computation
%-------------------------------------------------------------------------------
h= waitbar(0, ['Computing potential mapping with relaxation method (', mode, ')']);
% first loop (necessary because vectError has to be initialized)
waitbar(curLoop/maxLoop,h);
oldPotential= curPotential;
curPotential([2:x-1],[2:y-1],[2:z-1])= ...
    (oldPotential([1:x-2],[2:y-1],[2:z-1]) + oldPotential([3:x],[2:y-1],[2:z-1])...
    + oldPotential([2:x-1],[1:y-2],[2:z-1]) + oldPotential([2:x-1],[3:y],[2:z-1])...
    + oldPotential([2:x-1],[2:y-1],[1:z-2]) + oldPotential([2:x-1],[2:y-1],[3:z])...
    + q([2:x-1],[2:y-1],[2:z-1])) * coeff;
% Fixing of border conditions
curPotential= (curPotential.*notFrontier) + voltFrontier;
curError= sum(sum(sum(abs(curPotential-oldPotential))))/nbPoints;
vectError(curLoop)= curError;
curLoop= curLoop+1;

% any other loops
while ((curLoop<maxLoop) & continueError)
    waitbar(curLoop/maxLoop,h);
    oldPotential= curPotential;
    curPotential([2:x-1],[2:y-1],[2:z-1])= ...
        (oldPotential([1:x-2],[2:y-1],[2:z-1]) + oldPotential([3:x],[2:y-1],[2:z-1])...
        + oldPotential([2:x-1],[1:y-2],[2:z-1]) + oldPotential([2:x-1],[3:y],[2:z-1])...
        + oldPotential([2:x-1],[2:y-1],[1:z-2]) + oldPotential([2:x-1],[2:y-1],[3:z])...
        + q([2:x-1],[2:y-1],[2:z-1])) * coeff;

    % Fixing of border conditions
    curPotential= (curPotential.*notFrontier) + voltFrontier;
    curError= sum(sum(sum(abs(curPotential-oldPotential))))/nbPoints;
    vectError(curLoop)= curError;
    continueError= abs(curError- vectError(curLoop-1)) > differentialError;
    curLoop= curLoop+1;
end
close(h);

%=======================================================================%
% REMARKS: -------------------------------------------------------------%
%=======================================================================%
%  Poisson equation :                                                   %
%	d�U/dx� + d�U/dy� + d�u/dz�= - p                                    %
%   with the finite difference method                                   %
%                                                                       %
%	where U (x,y,z) is defined for                                      %
%		xi=x0+Delta(j) with j=0,1,...,J  horizontal                     %
%		yi=y0+Delta(l) with l=0,1,...,L  vertical                       %
%       zi=z0+Delta(z) with z=0,1,...,z  depth                          %
%       Delta is the grid size (equal to 1 here)                        %
%	Hence [U(j+1,l,z)-2*U(j,l,z)+U(j-1,l,z)]/(Deltay^2) +               %
%         [U(j,l+1,z)-2*U(j,l,z)+U(j,l-1,z)]/(Deltax^2) +               %
%         [U(j,l,z+1)-2*U(j,l,z)+U(j,l,z-1)]/(Deltaz^2) = - p(x,y,z)    %
%=======================================================================%

%=======================================================================%
%U(j,l,z)={[p(x,y,z)*[Deltax*Deltay*Deltaz]^2] +                        %
%         (Deltax^2*Deltaz^2)*[U(j+1,l,z)+ U(j-1,l,z)] +                %
%         (Deltay^2*Deltaz^2)*[U(j,l+1,z)+ U(j,l-1,z)] +                %
%         (Deltay^2*Deltax^2)*[U(j,l,z+1)+ U(j,l,z-1)] }                %
%        /(2*(Deltax^2*Deltaz^2+Deltay^2*Deltaz^2+Deltax^2*Deltay^2)    %
%=======================================================================%
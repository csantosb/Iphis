function [oldPotential, vectError, omega]= sor(oldPotential, voltFrontier, ...
    notFrontier, q, mode, curLoop, omega)
%sor - resolution of Poisson equation for a given geometry by
%   Successive Over Relaxation method with Chebysheff acceleration
%       o oldPotential  previously computed potential or initial potential
%       (ie anode) - 3D matrix
%       o voltFrontier  voltage at the frontier (anode, cathode, electrode
%       and package) - 3D matrix
%       o notFrontier   volume not belonging to a frontier (as above +
%       caspule) - 3D matrix
%       o q         relative charge in the whole volume - 3D matrix
%       o maxError  condition to end the computation
%       o curLoop   starting loop (if =0, omega is special)
%       o omega     convergence factor
% Outputs
%       o oldPotential  last computed potential
%       o vectError     error vector for the computed loops
%       o omega         convergence factor
% See remarks at the end of file for an explanation on the method SOR
% Mediocre results but fast convergence rate (but no convergence in the end),
% see sor.m for a more accurate method

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.1 $Date: december 2005/february 2006$

% Revision 1.1: upgraded ending condition with a test on the error evolution

%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
global gPotential
if strcmp(mode, 'Fast')
    maxLoop= 301;   % the computation will stop even if the error is huge
else 
    maxLoop= 3000;  % the computation will stop according to the error test
end
gPotential= oldPotential;
[x y z]= size(oldPotential);
jacobiRadius= (cos(pi/x)+cos(pi/y)+cos(pi/z))/3;
jacobiCoeff= 0.25*jacobiRadius^2;
dimCoeff= 1/6;
nbPoints= 1; %length(find(q~=0)); % includes detector(densge) and isolant
vectError= [];
if max(max(max(oldPotential)))== 1 % its Laplace equation for weighting field
    differentialError= 1e-12;      % when the difference between step n and n+1 is
else                             % below differentialError, stop computation
    differentialError= 1e-8;
end
continueError= 1;

%-------------------------------------------------------------------------------
%- Red/Black matrices
%-------------------------------------------------------------------------------
% build odd/even vectors: a point belongs to odd vector id sum(indexes) is
% odd
xVEven= [2:2:2*(fix((x-1)/2))];
xVOdd= [3:2:(2*(fix((x-2)/2))+1)];
yVEven= [2:2:2*(fix((y-1)/2))];
yVOdd= [3:2:(2*(fix((y-2)/2))+1)];
zVEven= [2:2:2*(fix((z-1)/2))];
zVOdd= [3:2:(2*(fix((z-2)/2))+1)];
% checkboard
% used to avoid loops (usually not fast in matlab)
% used to help debugging
V11= xVOdd;     V21= yVOdd;     V31= zVOdd;     Q1= q(V11,V21,V31);
V12= xVEven;    V22= yVEven;    V32= zVOdd;     Q2= q(V12,V22,V32);
V13= xVOdd;     V23= yVEven;    V33= zVEven;    Q3= q(V13,V23,V33);
V14= xVEven;    V24= yVOdd;     V34= zVEven;    Q4= q(V14,V24,V34);
V15= xVOdd;     V25= yVEven;    V35= zVOdd;     Q5= q(V15,V25,V35);
V16= xVEven;    V26= yVOdd;     V36= zVOdd;     Q6= q(V16,V26,V36);
V17= xVOdd;     V27= yVOdd;     V37= zVEven;    Q7= q(V17,V27,V37);
V18= xVEven;    V28= yVEven;    V38= zVEven;    Q8= q(V18,V28,V38);

%-------------------------------------------------------------------------------
%- First loop (omega has a special value)
%-------------------------------------------------------------------------------
h= waitbar(0, ['Computing potential mapping with sor method (', mode, ')']);

if curLoop==1
    om= dimCoeff*omega; 
    % Odd checkboard (first plane) - first 1/4
    computequart(Q1, V11, V21, V31, om);
    % Odd checkboard (first plane) - second 1/4
    computequart(Q2, V12, V22, V32, om);
    % Odd checkboard (second plane) - third 1/4
    computequart(Q3, V13, V23, V33, om);
    % Odd checkboard (second plane) - fourth 1/4
    computequart(Q4, V14, V24, V34, om);
    % Updating frontier fixed values
    gPotential= (gPotential.*notFrontier) + voltFrontier;
    % Omega update
    omega= 1/(1-2*jacobiCoeff);
    om= dimCoeff*omega;
    % Even checkboard (first plane) - first 1/4
    computequart(Q5, V15, V25, V35, om);
    % Even checkboard (first plane) - second 1/4
    computequart(Q6, V16, V26, V36, om);
    % Even checkboard (second plane) - third 1/4
    computequart(Q7, V17, V27, V37, om);
    % Even checkboard (second plane) - fourth 1/4
    computequart(Q8, V18, V28, V38, om);
    % Updating frontier fixed values
    gPotential= (gPotential.*notFrontier) + voltFrontier;
    % Omega update
    omega= 1/(1-jacobiCoeff*omega);
    curError= sum(sum(sum(abs(gPotential-oldPotential))))/nbPoints;
    vectError(curLoop)= curError;
    curLoop= curLoop+1;
end

while ((curLoop<maxLoop)& continueError)
    waitbar(curLoop/maxLoop,h);
    
    oldPotential= gPotential;
    % Omega update
    om= dimCoeff*omega;
    % Odd checkboard (first plane) - first 1/4
    computequart(Q1, V11, V21, V31, om);
    % Odd checkboard (first plane) - second 1/4
    computequart(Q2, V12, V22, V32, om);
    % Odd checkboard (second plane) - third 1/4
    computequart(Q3, V13, V23, V33, om);
    % Odd checkboard (second plane) - fourth 1/4
    computequart(Q4, V14, V24, V34, om);
    % Updating frontier fixed values
    gPotential= (gPotential.*notFrontier) + voltFrontier;
    
    % Omega update
    omega= 1/(1-jacobiCoeff*omega);
    om= dimCoeff*omega;
    % Even checkboard (first plane) - first 1/4
    computequart(Q5, V15, V25, V35, om);
    % Even checkboard (first plane) - second 1/4
    computequart(Q6, V16, V26, V36, om);
    % Even checkboard (second plane) - third 1/4
    computequart(Q7, V17, V27, V37, om);
    % Even checkboard (second plane) - fourth 1/4
    computequart(Q8, V18, V28, V38, om);
    % Updating frontier fixed values
    gPotential= (gPotential.*notFrontier) + voltFrontier;

    % Omega update
    omega= 1/(1-jacobiCoeff*omega);
    curError= sum(sum(sum(abs(gPotential-oldPotential))))/nbPoints;
    vectError(curLoop)= curError;
    continueError= abs(curError- vectError(curLoop-1)) > differentialError;
    curLoop= curLoop+1;       
end
oldPotential= gPotential;
close(h)
    
clear global gPotential

%===============================================================================
function computequart(q, x, y, z, omega)
global gPotential
tmp= gPotential(x-1,y,z)+gPotential(x+1,y,z)+gPotential(x,y-1,z)+gPotential(x,y+1,z)+...
    gPotential(x,y,z-1)+gPotential(x,y,z+1);
tmp= tmp - 6*gPotential(x,y,z) + q;
gPotential(x, y, z)= gPotential(x,y,z) + omega*tmp;


%=======================================================================%
% REMARKS: -------------------------------------------------------------%
%=======================================================================%
%  Equation is of type :                                                %
%	d�U/dx� + d�U/dy� + d�u/dz�= - p                                    %
%    with finite difference method                                      %
%                                                                       %
%	where U(x,y,z) is defined as                                        %
%		xi= x0+Delta(j) with j=0,1,...,J  horizontal                    %
%		yi= y0+Delta(l) with l=0,1,...,L  vertical                      %
%       zi= z0+Delta(z) with z=0,1,...,Z  depth                         %
%       Delta is the grid size                                          %
%	On a  [U(j+1,l,z)-2*U(j,l,z)+U(j-1,l,z)]/(Deltay^2) +               %
%         [U(j,l+1,z)-2*U(j,l,z)+U(j,l-1,z)]/(Deltax^2) +               %
%         [U(j,l,z+1)-2*U(j,l,z)+U(j,l,z-1)]/(Deltaz^2) = - p(x,y,z)    %
%=======================================================================%

%=======================================================================%
%U(j,l,z)={[p(x,y,z)*[Deltax*Deltay*Deltaz]^2] +                        %
%         (Deltax^2*Deltaz^2)*[U(j+1,l,z)+ U(j-1,l,z)] +                %
%         (Deltay^2*Deltaz^2)*[U(j,l+1,z)+ U(j,l-1,z)] +                %
%         (Deltay^2*Deltax^2)*[U(j,l,z+1)+ U(j,l,z-1)] }                %
%        /(2*(Deltax^2*Deltaz^2+Deltay^2*Deltaz^2+Deltax^2*Deltay^2)    %
%=======================================================================%


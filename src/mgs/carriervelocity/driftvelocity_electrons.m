function dve= driftvelocity_electrons(efield_x,efield_y,efield_z,temperature,...
    phi, ppath)
%driftvelocity_electrons - compute the drift velocity of electrons
%       o efield_x  electric field in x axis
%       o efield_y  electric field in y axis
%       o efield_z  electric field in z axis
%       o temperature   in Kelvin
%       o phi   rotation angle of the main anisotropic axis
% Outputs
%       o dve
% See remarks at the end of file for an explanation on the method

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% TODO: clean up



    global Gj n_valleys    

    n_valleys=4;                            % number of valleys
    m_t=0.0819;                             % effective mass in <100>
    m_l=1.64;                               % effective mass in <010>
    Beta=acos(sqrt(2/3));                   % rotation angle to point to <111> or equivalent
    Rx=[   1           0           0;
           0       cos(Beta)   -sin(Beta);
           0       sin(Beta)    cos(Beta)];
    gamma_0=[   1/m_t     0           0;    % diagonal effective mass matrix
                  0       1/m_l       0;
                  0       0         1/m_t];

    for j=1:n_valleys                       % index pointing to the j-th valley
        alfa=(pi/4)+(j-1)*(pi/2);           % rotation angle around z axis to j-th valley
        Rz=[cos(alfa)   -sin(alfa)     0;
            sin(alfa)    cos(alfa)     0;
               0           0          1];
        R_j=Rx*Rz;
        Gj{j}=R_j^(-1)*gamma_0*R_j;         % effective tensor in the j-th valley
    end

% Electric field reshape
    efield=[reshape(efield_x,1,numel(efield_x));...     
            reshape(efield_y,1,numel(efield_y));...     
            reshape(efield_z,1,numel(efield_z))];       
    efield_norm=sqrt(dot(efield,efield,1));
    
% Rotation of the main anisotropy axis    
    Rotate_efield = [   cos(phi)    sin(phi)     0;
                        sin(-phi)   cos(phi)     0;
                            0           0        1];                        
    efield = Rotate_efield*efield;                   

% DRFT(4,:)
    for i=1:n_valleys,
        temp=(dot(efield,(Gj{i}*efield)));
        temp(find(temp~=0))=temp(find(temp~=0)).^(-0.5);
        drft(i,:)=temp;
    end

% DRFT_SUM(4,:)  adds the columns elements of DRIFT and replicates to keep
% same dimensions
    drft_sum_tmp=sum(drft,1);
    drft_sum=[];
    for i=1:n_valleys,
        drft_sum=cat(1,drft_sum,drft_sum_tmp);
    end
    clear drft_sum_tmp

% DUMMY2(4,:)  output from the R_E_111() function and replicates to keep
% same dimensions
    RE111=r_e_111(efield_norm,temperature, ppath).*(efield_norm~=0);            
    dummy2=[];
    for i=1:n_valleys,
        dummy2=cat(1,dummy2,RE111);
    end
    clear RE111

% DRFT(4,:)
    tmp=zeros(size(drft));
    tmp(:,find(efield_norm~=0))=drft(:,find(efield_norm~=0))./drft_sum(:,find(efield_norm~=0));
    drft=drft.*(dummy2.*(tmp-.25)+.25);
    clear dummy2 drft_sum

% DRFT_VEL(3,:)
    AE100=a_e_100(efield_norm,temperature, ppath).*(efield_norm~=0);
    drft_vel=zeros(size(efield));
    for j=1:n_valleys,
        drft_vel = drft_vel + (Gj{j}*efield).*[drft(j,:);drft(j,:);drft(j,:)];
    end
    drft_vel = drft_vel.*[AE100;AE100;AE100];
    
% Rotation of the main anisotropy axis    
    phi = -phi;
    Rotate_efield = [   cos(phi)    sin(phi)     0;
                        sin(-phi)   cos(phi)     0;
                            0           0        1];                        
    drft_vel = Rotate_efield*drft_vel;                   
    
% Drift velocity reshape
    dve.x=reshape(drft_vel(1,:),size(efield_x));         % Velocity  cm./s. 
    dve.y=reshape(drft_vel(2,:),size(efield_y));         % Velocity  cm./s. 
    dve.z=reshape(drft_vel(3,:),size(efield_z));         % Velocity  cm./s. 
    
    
% Compute the drift velocities of charge carriers (electrons) from an 
% externally applied electric field. The model used follows indications 
% extracted from L. Mihailescu et al., Nucl. Instr. and Meth. A 447 (2000) 
% 350-360.
% The function accepts cartesian coordinates of E in the form of three
% separate fields, which can be a n-dimensional matrix, each input
% representing a different space point. The arrays
% X,Y, and Z must be the same size. Output will have the same shape
% as input. 
% TEMPERATURE plays a main role in the model used.
% PHI is the rotation angle (with respect to the x>0 axis) of the <100>
%   main anisotropic axis
%   Units:
%   efield input is in Volts/ centimeter
%   drift velocities outputs are in centimeter/second
%   temperature is in Kelvin.
% 
%   See also SOLVE_DRIFT_VELOCITY TEST_ANISOTROPY TEST_ANISOTROPY2 A_E_100  R_E_111.

%   Written by C. SANTOS <cayetano.santos@netscape.net>
%   Date: 2004/02/01
    

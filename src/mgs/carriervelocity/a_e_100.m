function [a_e_100_out]=a_e_100(E,Temperature, ppath)
%A_E_100  Drift velocities from external electric field.
%   Experimental expression for drift velocities in <100>
%   Input must be a row vector with the electric field magnitude
%   Output is a row vector
% 
%   See also SOLVE_DRIFT_VELOCITY TEST_ANISOTROPY TEST_ANISOTROPY2 DRIFT_VELOCTITY  R_E_111.

%   Written by C. SANTOS <cayetano.santos@netscape.net>
%   Date: 2004/02/01
    
    global Gj n_valleys

    % fit parameters for the experimental drift velocity in the <100> direction
    %        /u_0[cm^2/Vs]                              Eo(V/cm]                /B                   /u_n[cm^2/Vs]             Direction
    %   dir_100.mu_0=mobility_100(Temperature);    dir_100.E_0=493;     dir_100.Beta=0.72;        dir_100.mu_n= 589;         %<100>
        load([ppath, 'driftparameters_electrons.mat'], 'dir_100');
    
    %  calculation of the term (0.25)*gamma_j*E0/(E0*gamma_j*E0)^.5 
    %  usefull when obtaining A(|E|)
        E02=[1 0 0]';
        term=zeros(3,1);
        for j=1:n_valleys,
            term=term+(Gj{j}*E02)*((E02'*Gj{j}*E02)^(-0.5))*.25;
        end
    % check out of validity whith theory: direction must form an angle of 0 degrees with x axis
        [TH,R,Z]=cart2pol(term(1),term(2),term(3));
        TH=(fix(TH*100))/100;
        Z=(fix(Z*100))/100;
        if ~(TH==0 & Z==0),disp('error in calculating A'),[TH,R,Z],return, end
        
        term=norm(term);
%         v_exp_100=((dir_100.mu_0*E)./((1+(E/dir_100.E_0).^dir_100.Beta).^(1/dir_100.Beta)))-dir_100.mu_n*E;
        v_exp_100=(E<1e4).*(((dir_100.mu_0*E)./((1+(E/dir_100.E_0).^dir_100.Beta).^(1/dir_100.Beta)))-dir_100.mu_n*E)+...
            (E>=1e4)*1.1e7;
        a_e_100_out=(v_exp_100/term)+eps*(E==0);    % avoid zeros to r_e_111
        
        
function  mobility=mobility_100(Temperature)
% Experimental expression for the mobility of ...
    Cte=40180/(78^(-1.5));
    mobility=Cte*(Temperature^(-1.5));        
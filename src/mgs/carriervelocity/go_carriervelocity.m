function go_carriervelocity(handles)
%go_carriervelocity - compute the drift velocity of hole and electron
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% TODO: check that CdTe is correctly working

%-------------------------------------------------------------------------------
%- load and save data
%-------------------------------------------------------------------------------
datapath= [handles.datapath, handles.detector, filesep];

location= [datapath,'geometry.mat'];
[filegeo, pathgeo]= uigetfile(location, 'Select your geometry file ... ');
if filegeo==0,return,else, load([pathgeo,filegeo],'impurities', 'detector'),end

location= [datapath,'efield.mat'];
[filename, pathname]= uigetfile(location,'Select your electric field file ... ');
if filename==0,return,else, load([pathname,filename],'efield_x','efield_y','efield_z'),end

datapath= [handles.datapath, handles.detector, filesep];
location= [datapath, 'drift_velocities.mat'];
[filename, pathname]= uiputfile(location, 'Save drift velocity data ... ');
if filename==0, return, end

%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
% efield_x= double(efield_x);
% efield_y= double(efield_y);
% efield_z= double(efield_z);

%-------------------------------------------------------------------------------
%- computation
%-------------------------------------------------------------------------------
if isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'silicium')
    fprintf('[Material is Silicium] Not implemented\n');
elseif isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'CdTe')
    fprintf('[Material is CdTe] Computing\n');
    load([filegeo, pathgeo],'Temperature');
    % Drift velocity for hole
    mu_h= mu_hole(Temperature);
    [dvh_x]= [efield_x]*mu_h;
    [dvh_y]= [efield_y]*mu_h;
    [dvh_z]= [efield_z]*mu_h;
    % Drift velocity for electron
    mu_e= mu_electron(Temperature);
    [dve_x]= [efield_x]*mu_e;
    [dve_y]= [efield_y]*mu_e;
    [dve_z]= [efield_z]*mu_e;
elseif (isfield(impurities,'material') & strcmp(getfield(impurities,'material'),'germanium')) ...
        | ~isfield(impurities,'material')
    %-------------------------------------------------------------------------------
    %- Drift velocity for Germanium detector (holes)
    %-------------------------------------------------------------------------------
    fprintf('[Material is Germanium]\n');
    % Holes
    fprintf('\tThis model follows indications extracted from B. Bruyneel\n');
    fprintf('Characterization of a 12-fold segmented MINIBALL detector, May, 2005.\n');

    answer=inputdlg('Enter the angle of anisotropy (holes, in degrees)', ...
        'Modificate default values ... ?',1,{num2str(0)});
    if isempty(answer),return,end
    phihole= eval(answer{1})*pi/180;

    answer=inputdlg('Enter the angle of anisotropy (electrons, in degrees)',...
        'Modificate default values ... ?',1,{num2str(0)});
    if isempty(answer),return,end
    phielectron= eval(answer{1})*pi/180;

    tic    
    if detector.step == 10 & prod(size(efield_x)) < 2000000 % 1mm grid size
        dvh= driftvelocity_holes(efield_x,efield_y,efield_z,78,phihole, ...
            handles.parameterpath);
    else
        h= waitbar(0,'Computing drift velocity for holes');
        nbLines= size(efield_x,3);
        for i = 1:nbLines
            dvholes= driftvelocity_holes(efield_x(:,:,i),efield_y(:,:,i),...
                efield_z(:,:,i),78,phihole, handles.parameterpath);
            dvh.x(:,:,i)= dvholes.x;
            dvh.y(:,:,i)= dvholes.y;
            dvh.z(:,:,i)= dvholes.z;
            waitbar((i/nbLines),h);
        end
        close(h);
    end
    if strcmp(handles.precision,'single')
        dvh.x= single(dvh.x);
        dvh.y= single(dvh.y);
        dvh.z= single(dvh.z);
    end
%    dvh.m= sqrt(dvh.x.^2+dvh.y.^2+dvh.z.^2);
    %-------------------------------------------------------------------------------
    %- save data
    %-------------------------------------------------------------------------------
    save([pathname,filename],'dvh') % dvh.m not saved
    clear dvh
    timeElapsed= toc;
    computationduration('Drift velocity for holes', timeElapsed);
    %-------------------------------------------------------------------------------
    %- Drift velocity for Germanium detector (electron)
    %-------------------------------------------------------------------------------
    tic
    fprintf('\tThis model follows indications extracted from L. Mihailescu\n');
    fprintf('\tet al., Nucl. Instr. and Meth. A 447 (2000) 350-360.\n');
    
    
    h = waitbar(0,'Computing drift velocity for electrons');
    nbLines= size(efield_x,3);
    for i = 1:nbLines
        if i==1,verbose=1;else,verbose=0;end
        dvelectrons= driftvelocity_electrons(-efield_x(:,:,i),...
            -efield_y(:,:,i),-efield_z(:,:,i),78,phielectron, handles.parameterpath); 
        dve.x(:,:,i)= dvelectrons.x;
        dve.y(:,:,i)= dvelectrons.y;         
        dve.z(:,:,i)= dvelectrons.z;         
        waitbar(i/nbLines,h);
    end
    close(h)
    if handles.precision=='single'
        dve.x=single(dve.x);
        dve.y=single(dve.y);
        dve.z=single(dve.z);
    end
%    dve.m=sqrt(dve.x.^2+dve.y.^2+dve.z.^2);
    
    save([pathname,filename],'dve','-append')
    timeElapsed= toc;
    computationduration('Drift velocity for holes', timeElapsed);
else
    fprintf('[Material is unknow] Error\n');
end

update_Callback(handles);


%===============================================================================
function mu_e= mu_electron(Temperature)
mu_e0= 20;   % units
T0= 298;     % K�
constant= 5; % units
mu_e= mu_e0+constant*(Temperature-T0);
mu_e= -mu_e;
%===============================================================================
function mu_h= mu_hole(Temperature)
mu_h0= 20;   % units
T0= 298;     % K�
constant= 5; % units
mu_h= mu_h0+constant*(Temperature-T0);
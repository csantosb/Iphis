function [r_e_111_out]=r_e_111(E,Temperature, ppath)
%R_E_1111  Drift velocities from external electric field.
%   Experimental expression for drift velocities in <111>
%   Input must be a row vector with the electric field magnitude
%   Output is a row vector
% 
%   See also SOLVE_DRIFT_VELOCITY TEST_ANISOTROPY TEST_ANISOTROPY2 DRIFT_VELOCTITY  A_E_100.

%   Written by C. SANTOS <cayetano.santos@netscape.net>
%   Date: 2004/02/01

    global Gj n_valleys

    % fit parameters for the experimental drift velocity in the <111> direction
    %        /u_0[cm^2/Vs]                              Eo(V/cm]                /B                   /u_n[cm^2/Vs]             Direction
    %   dir_111.mu_0=mobility_111(Temperature);    dir_111.E_0=251;     dir_111.Beta=0.87;        dir_111.mu_n= 62;          %<111>
    load([ppath, 'driftparameters_electrons.mat'], 'dir_111');
    
    %  calculation of the terms 
    %  ------------------- 
    %  usefull when obtaining R(|E|)
        E02=[1 1 1]';
        TMP1=zeros(3,1);
        TMP2=zeros(3,1);
        TMP3=term_b(E02);
        
    for j=1:n_valleys
        tmp=((E02'*Gj{j}*E02)^(-0.5))/TMP3-0.25;
        tmp=tmp*((E02'*Gj{j}*E02)^(-0.5));
        tmp=tmp*(Gj{j}*E02);
        TMP1=TMP1+tmp;
        tmp2=((E02'*Gj{j}*E02)^(-0.5))*(Gj{j}*E02)*.25;
        TMP2=TMP2+tmp2;
    end
    TM1=norm(TMP1);
    TM2=norm(TMP2);

%     v_exp_111=((dir_111.mu_0*E)./((1.+(E/dir_111.E_0).^dir_111.Beta).^(1/dir_111.Beta)))-dir_111.mu_n*E;
    v_exp_111=(E<1e4).*(((dir_111.mu_0*E)./((1+(E/dir_111.E_0).^dir_111.Beta).^(1/dir_111.Beta)))-dir_111.mu_n*E)+...;
    (E>=1e4)*0.95e7;
%     v_exp_111=((dir_111.mu_0*E)./((1+(E/dir_111.E_0).^dir_111.Beta).^(1/dir_111.Beta)))-dir_111.mu_n*E;
    
%     warning off MATLAB:divideByZero
    r_e_111_out=(-1/TM1)*(v_exp_111./a_e_100(E,Temperature, ppath)-TM2);
    r_e_111_out=r_e_111_out.*(E~=0);
%     warning on MATLAB:divideByZero
    
function b=term_b(E0)
    global Gj n_valleys 
    b=0;
    for j=1:n_valleys
        b= b + ((E0'*Gj{j}*E0)^(-0.5));
    end
    
function  mobility=mobility_111(Temperature)
% Experimental expression for the mobility of ...
    Cte=42420/(78^(-1.5));
    mobility=Cte*(Temperature^(-1.5));
    

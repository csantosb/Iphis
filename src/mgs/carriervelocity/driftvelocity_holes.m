function dvh= driftvelocity_holes(efield_x,efield_y,efield_z,Temperature,phi, ...
    ppath)
%driftvelocity_holes - compute the drift velocity of holes
%       o efield_x  electric field in x axis
%       o efield_y  electric field in y axis
%       o efield_z  electric field in z axis
%       o temperature   in Kelvin
%       o phi   rotation angle of the main anisotropic axis
%       o ppath path find driftvelocity_holes.mat
% Outputs
%       o dvh
% See remarks at the end of file for an explanation on the method

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$



% TODO: clean up



% Electric field reshape
    efield=[reshape(efield_x,1,numel(efield_x));...     
            reshape(efield_y,1,numel(efield_y));...     
            reshape(efield_z,1,numel(efield_z))];       
    efield_norm = sqrt(dot(efield,efield,1));

    
% Rotation of the main anisotropy axis    
    Rotate_efield = [   cos(phi)    sin(phi)     0;
                        sin(-phi)   cos(phi)     0;
                            0           0        1];                        
    efield = Rotate_efield*efield;                   
    
% Goes from cartesian to cylindrical coordinates  
    R = sqrt(efield(1,:).^2 + efield(2,:).^2 + efield(3,:).^2);
    PHI = atan2 (eps+efield(2,:),eps+efield(1,:));
    PHI(find(PHI<0)) = PHI(find(PHI<0)) + 2*pi;
    TH = acos ((eps+efield(3,:))./(eps+R));

% fit parameters for the experimental drift velocities in the <100> and
% <111> directions (Reggiani, Canali, Nava)
%        /u_0[cm^2/Vs]              Eo(V/cm]            /B                   /u_n[cm^2/Vs]             Direction
%        dir_100.mu_0=66333;    dir_100.E_0=181;     dir_100.Beta=0.744;        dir_100.mu_n= 0;         %<100>
%        dir_111.mu_0=107270;    dir_111.E_0=100;     dir_111.Beta=0.580;        dir_111.mu_n= 0;          %<111>

% fit parameters for the experimental drift velocities in the <100> and
% <111> directions (B. Bruyneel)
%        /u_0[cm^2/Vs]              Eo(V/cm]            /B                   /u_n[cm^2/Vs]             Direction
%        dir_100.mu_0=61824;    dir_100.E_0=185;     dir_100.Beta=0.942;        dir_100.mu_n= 0;         %<100>
%        dir_111.mu_0=61215;    dir_111.E_0=182;     dir_111.Beta=0.662;        dir_111.mu_n= 0;          %<111>

% Loads fit parameters from a file
    load([ppath, 'driftparameters_holes.mat']);
    
% Calculates vrel=v_111/v_100 in order to stimate ko and then alpha and beta     
    v_100=((dir_100.mu_0*efield_norm)./((1.+(efield_norm./dir_100.E_0).^dir_100.Beta).^(1/dir_100.Beta)))-dir_100.mu_n*efield_norm;
    v_111=((dir_111.mu_0*efield_norm)./((1.+(efield_norm./dir_111.E_0).^dir_111.Beta).^(1/dir_111.Beta)))-dir_111.mu_n*efield_norm;

    vrel = zeros(size(efield_norm));
    vrel(:,find(efield_norm~=0)) = v_111(:,find(efield_norm~=0)) ./ v_100(:,find(efield_norm~=0));
    
    ko = 9.2652 - 26.3467 .* vrel + 29.6137 .* vrel.^2 - 12.3689 .* vrel.^3;%ko = ko_de_vrel(vrel);

    alpha_ko = -0.01322.*ko + 0.41145 .* ko.^2 - 0.23657 .* ko.^3 + 0.04077 .* ko.^4;%alpha_ko = alpha_de_ko(ko);
    beta_ko = 0.006550 .* ko - 0.19946 .* ko.^2 + 0.09859 .* ko.^3 - 0.01559 .* ko.^4;%beta_ko = beta_de_ko(ko);

% Evaluates the solution in spherical coordinates    
    v_r = v_100 .* (1 - alpha_ko .* (  sin(TH).^4 .* sin(2.*PHI).^2 + sin(2.*TH).^2 ));
    v_phi = v_100 .* beta_ko .* sin(TH).^3 .* sin(4*PHI);
    v_th = v_100 .* beta_ko .* ( 2.*sin(TH).^3 .* cos(TH) .* sin(2.*PHI).^2 + sin(4.*TH));
    
    v = [v_r; v_th; v_phi];
    
    v=repmat(v,[1 1 3]);
    v=permute(v,[1,3,2]);
    v=permute(v,[2,1,3]);
        
    T = zeros(3,3,length(v_r));
%     Cart = zeros(3,length(v_r));
    
    T(1,1,:) = sin(TH).*cos(PHI);
    T(1,2,:) = cos(TH).*cos(PHI);
    T(1,3,:) = -sin(PHI);
    T(2,1,:) = sin(TH).*sin(PHI);
    T(2,2,:) = cos(TH).*sin(PHI);
    T(2,3,:) = cos(PHI);
    T(3,1,:) = cos(TH);
    T(3,2,:) = -sin(TH);
    T(3,3,:) = 0;
    
    Cart = T.*v;
    
    Cart = squeeze(sum(Cart,2));
    
% Rotation of the main anisotropy axis    
    phi = -phi;
    Rotate_efield = [   cos(phi)    sin(phi)     0;
                        sin(-phi)   cos(phi)     0;
                            0           0        1];                        
    Cart = Rotate_efield*Cart;                   
        
    dvh.x=reshape(Cart(1,:),size(efield_x));         % Velocity  cm./s. 
    dvh.y=reshape(Cart(2,:),size(efield_y));         % Velocity  cm./s. 
    dvh.z=reshape(Cart(3,:),size(efield_z));         % Velocity  cm./s. 
    
% Useful for tests  _____________________________________________________   
    
%     dvh.v_r = v_r;
%     dvh.v_phi = v_phi;
%     dvh.v_th = v_th;
% 
%     dvh.T = T;

%===============================================================================
function alpha_ko = alpha_de_ko(ko)
    alpha_ko = -0.01322.*ko + 0.41145 .* ko.^2 - 0.23657 .* ko.^3 + 0.04077 .* ko.^4;
%===============================================================================
function beta_ko = beta_de_ko(ko)
    beta_ko = 0.006550 .* ko - 0.19946 .* ko.^2 + 0.09859 .* ko.^3 - 0.01559 .* ko.^4;
%===============================================================================
function ko_vrel = ko_de_vrel(vrel)
    ko_vrel = 9.2652 - 26.3467 .* vrel + 29.6137 .* vrel.^2 - 12.3689 .* vrel.^3;

    
    %DRIFT_VELOCITY_HOLES  Drift velocities from external electric field.
%   DRIFT_VELOCITY__HOLES(efield_x,efield_y,efield_z,Temperature,phi,verbose) calculates the
%   drift velocities of charge carriers (holes) from an externally applied electric
%   field. The model used follows indications extracted from B. Bruyneel
%   “Characterization of a 12-fold segmented MINIBALL detector”, May, 2005.
%   The function accepts cartesian coordinates of E in the form of three
%   separate fields, which can be a n-dimensional matrix, each input
%   representing a different space point. The arrays
%   X,Y, and Z must be the same size. Output will have the same shape
%   as input. 
%   TEMPERATURE plays a main role in the model used.
%   PHI is the rotation angle (with respect to the x>0 axis) of the <100>
%   main anisotropic axis
%   Units:
%   efield input is in Volts/ centimeter
%   drift velocities outputs are in centimeter/second
%   temperature is in Kelvin.
% 
%   See also SOLVE_DRIFT_VELOCITY TEST_ANISOTROPY TEST_ANISOTROPY2 A_E_100  R_E_111.

%   Written by C. SANTOS <cayetano.santos@netscape.net>
%   Date: 2005/13/07
    
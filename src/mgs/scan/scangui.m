function scangui(handles)
%scangui -  launch scan for a a list a points
%       o handles   structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$


% check that a detector has been selected
if handles.detector(1)== '-'
    msgbox('Detector not selected', 'Please select a detector');
    return
end

%-------------------------------------------------------------------------------
%- build interaction points coordinates
%-------------------------------------------------------------------------------
askQuestion= questdlg('Load points to scan from file?',' ','yes','no','exit','no');

if strcmp(askQuestion,'yes')
    %   Points to scan are read from a three columns, ascii file
    location=([handles.currentpath,'points.txt']);
    [filename, pathname]=uigetfile(location,'Select your scanning points file ... ');
    if filename==0,return,else,end
    fid= fopen([pathname,filename],'r');
    data= fscanf(fid,'%c',5);
    if strcmp(data,'x y z'),
        set(handles.scancartesian, 'Value', 1);
        set(handles.scancylindrical, 'Value', 0);
        handles.scancoord= 'cartesian'; 
        fprintf('[Scan] Points are in cartesian coordinates...\n')
    elseif strcmp(data,'r t z'),
        set(handles.scancartesian, 'Value', 0);
        set(handles.scancylindrical, 'Value', 1);
        handles.scancoord= 'cylindrical'; 
        fprintf('[Scan] Points are in cylindrical coordinates...\n')
    end
    IPCoord=[];
    while ~isempty(data)
        data= fscanf(fid,'%g %g %g',[1,3]);
        IPCoord= [IPCoord; data];
    end
    fclose(fid);
elseif strcmp(askQuestion,'no')
    IPCoord= buildIP(handles, strcmp(handles.scancoord, 'cartesian'));
else
    return,
end

if ~isempty(IPCoord)
    scan(handles, IPCoord, 'gui');
end

%===============================================================================
function IPCoord= buildIP(handles, cartesian)
IPCoord= [];
load([handles.datapath, handles.detector, filesep, 'geometry.mat'], 'detector', 'densge');
if cartesian
    prompt={'x(min)','x(step)','x(max)', 'y(min)','y(step)','y(max)',...
        'z(min)','z(step)','z(max)'};
    def={'1', '5', int2str(detector.length*detector.step/10) ,...
        '1', '5', int2str(detector.height*detector.step/10) ,...
        '1', '5', int2str(detector.depth*detector.step/10)};
else
    prompt={'r(min)','r(step)','r(max)', 'theta(min)','theta(step)','theta(max)',...
        'z(min)','z(step)','z(max)'};
    def={'0', int2str(round(detector.length/10)), int2str(round(detector.length/2)) ,...
        '0', '10', int2str(360),...
        '1', '5', int2str(detector.depth*detector.step/10)};
end

answer= inputdlg(prompt,'Input for scan (mm, degre)...',1,def);
if isempty(answer),return,end


if cartesian
    fprintf('x in [%s:%s:%s], y in [%s:%s:%s], z in [%s:%s:%s]\n', ...
        answer{1}, answer{2}, answer{3},answer{4}, answer{5}, answer{6}, ...
        answer{7}, answer{8}, answer{9});
else
    fprintf('r in [%s:%s:%s], theta in [%s:%s:%s], z in [%s:%s:%s]\n', ...
        answer{1}, answer{2}, answer{3},answer{4}, answer{5}, answer{6}, ...
        answer{7}, answer{8}, answer{9});
end

savedata = questdlg('Select between "scan only" or "save points and scan"',...
    'Scan','Scan + Save points','Scan','Cancel','Scan');
if isempty(savedata) | strcmp(savedata, 'Cancel'),
    return,
end

for xi= eval(answer{1}):eval(answer{2}):eval(answer{3})
    for yi= eval(answer{4}):eval(answer{5}):eval(answer{6})
        for zi= eval(answer{7}):eval(answer{8}):eval(answer{9})
            IPCoord= [IPCoord;xi yi zi];
        end
    end
end

% remove coordinates which are not in the volume (save memory)
% ind= [];
% notdensge= (densge~=0);
% [xg,yg,zg]= meshgrid([1:dimension.nwcols],[1:dimension.nrows],[1:dimension.nprofs]);

% if cartesian
%     for i=1:size(IPCoord,1)
%         if round(interp3(xg,yg,zg,notdensge,IPCoord(i,1),IPCoord(i,2),IPCoord(i,3)))==1
%             ind= [ind; i];
%         end
%     end
% else % cylindrical
%     for i=1:size(IPCoord,1)
%             [x,y]= pol2cart(IPCoord(2,i)*(pi/180),IPCoord(1,i)*(10/detector.step));
%     x= x+vide.left+(detector.length+1)/2;
%     y= y+vide.up  +(detector.height+1)/2;
%     z= z*(10/detector.step)+vide.forward;
%         
%         if round(interp3(xg,yg,zg,notdensge,IPCoord(i,1),IPCoord(i,2),IPCoord(i,3)))==1
%             ind= [ind; i];
%         end
%     end
% end
% for i=1:size(volume,1)
%     if fix(interp3(xg,yg,zg,notdensge,volume(i,1),volume(i,2),volume(i,3)))==1
%         ind= [ind; i];
%     end
% end
% volume= volume(ind,:);


% save generated IPCoordinates
if strcmp(savedata, 'Scan + Save points')
    location= [handles.currentpath, handles.detector, '_points.txt'];
    [filename, pathname]= uiputfile(location, 'Save generated points');
    if filename==0, 
        IPCoord= [];
        return, 
    end
    fid= fopen([pathname, filename],'w');
    if cartesian
        fprintf(fid,'x y z\n');
    else
        fprintf(fid,'r t z\n');
    end
    fprintf(fid,'%2.2f %2.2f %2.2f\n',IPCoord');

    fclose(fid);
    fprintf('[Scan] Generated points saved in %s, %d points\n', filename, size(IPCoord,1));
end

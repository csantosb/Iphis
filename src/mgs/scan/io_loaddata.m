function data= io_loaddata(handles, x,y,z, dataformat, nbsegment, type)
%io_loaddata - load pulses from one file
% Inputs
%       o handles   structure with handles and user data (see GUIDATA)
%       o x         first coordinate of the interaction point
%       o y         second coordinate
%       o z         third coordinate
%       o dataformat  description of the data format
%       o type      'current' or 'charge'
% Outputs
%       o data      pulses for each segments (current or charge) 

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

datapath= [handles.datapath, handles.detector, filesep, type, filesep];
if strcmp(handles.scancoord, 'cartesian')
    filename= ['z',num2str(z),filesep,type,'_x',num2str(x),'_y',num2str(y)];
else
    filename= ['z',num2str(z),filesep,type,'_r',num2str(x),'_t',num2str(y)];
end
mode= get(handles.scanbinary, 'Value');
if strcmp(type, 'charge')
    shape= [1+nbsegment inf];
else
    shape= [7+2*(nbsegment) inf];
end
if mode % binary
    fid= fopen([datapath, filename, '.bin'],'r');
    if fid==-1,
        fprintf('[Load file] %s.bin not found\n', filename);
        data= [];
        return
    end
    data= fread(fid,'single');
    m= nbsegment+1; % vectortime + segments
    n= length(data)/m;
    data= reshape(data, n, m);
else    % ascii
    fid= fopen([datapath, filename, '.txt'], 'r');
    if fid==-1,
        fprintf('[Load file] %s.txt not found\n', filename);
        data= [];
        return
    end
    data= fscanf(fid,'%g',shape);
    data= data';
end
fclose(fid);

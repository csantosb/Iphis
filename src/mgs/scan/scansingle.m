function scansingle(handles, mode)
%scansingle -  launch scan for a single point
%       o handles   structure with handles and user data (see GUIDATA)
%       o mode      computation of trajectories, or pulse shapes

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% check that a detector has been selected
if handles.detector(1)== '-'
    msgbox('Detector not selected', 'Please select a detector');
    return
end

IPCoord= [str2num(get(handles.scanx, 'String')), ...
    str2num(get(handles.scany, 'String')), ...
    str2num(get(handles.scanz, 'String'))];
switch (mode)
    case 'e'
        computetrajectory('init', handles);
        if computetrajectory('outside',IPCoord(1), IPCoord(2), IPCoord(3), 1)     
            fprintf('[%3.1f %3.1f %3.1f] to close to an electrode\n',...
                IPCoord(1), IPCoord(2), IPCoord(3));
        else
             computetrajectory('calc', IPCoord(1), IPCoord(2), IPCoord(3), 1, 1);
        end
        computetrajectory('end');             
    case 'h'
        computetrajectory('init', handles);
        if computetrajectory('outside',IPCoord(1), IPCoord(2), IPCoord(3), 1)     
            fprintf('[%3.1f %3.1f %3.1f] to close to an electrode\n',...
                IPCoord(1), IPCoord(2), IPCoord(3));
        else
             computetrajectory('calc', IPCoord(1), IPCoord(2), IPCoord(3), 1, 0);
        end
        computetrajectory('end');          
    case 'pulse'
        scan(handles, IPCoord, 'single');
    otherwise
        fprintf('[ERROR] %s is not defined for single scan\n', mode);
end

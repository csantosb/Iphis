function traj= computetrajectory(mode, arg1, arg2, arg3, verbose, element)
%computetrajectory -  compute the trajectory of an element (hole or
%electron)
%       o mode  'init', 'outside', 'calc' or 'end' to respectively initialize 
%           the computations, check if the point is inside the volume,
%           compute the trajectory and finally finalize the computation
%       o arg1  depends on the mode. Can be handles or first coordinate
%       o arg2  second coordinate
%       o arg3  third coordinate
%       o verbose if equal to 1, plot and text output are enabled
%       o element hole (0) or electron (1)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% TODO: check outside limits (should be ok for points very close to the
% border)
% Check end of traj

global cartesian cathode anode detector vide stepTime voltage potential scale notElect displayTraj
global inDetector dvhx dvhy dvhz dvex dvey dvez x y z densge
global mgsversion geometryName  

switch (mode)
%-------------------------------------------------------------------------------
%- initialize computation (done once per scan)
%-------------------------------------------------------------------------------
    case 'init'
        geometryPath= [arg1.datapath,arg1.detector,filesep];
        stepTime= arg1.steptime;
        load([geometryPath,'geometry.mat']);
        cathode= single(cathode);
        anode= single(anode);
        load([geometryPath,'drift_velocities.mat']);
        load([geometryPath,'potential_mapping.mat']);
        scale= 100/detector.step;
        % copy velocity matrices
        dvex= scale*dve.x;
        dvey= scale*dve.y;
        dvez= scale*dve.z;
        dvhx= scale*dvh.x;
        dvhy= scale*dvh.y;
        dvhz= scale*dvh.z;
        [x,y,z]= meshgrid([1:dimension.nwcols],[1:dimension.nrows],[1:dimension.nprofs]);
        cartesian= strcmp(arg1.scancoord, 'cartesian');
        displaytraj= arg1.plotenable;
        inDetector= (densge~=0);
        mgsversion= [arg1.version, arg1.releasedate];
        if voltage.cathode < voltage.anode
            voltage.min= voltage.cathode;
            voltage.max= voltage.anode;
        else
            voltage.min= voltage.anode;
            voltage.max= voltage.cathode;
        end
    case 'outside'
%-------------------------------------------------------------------------------
%- check if the points belongs to the volume
%-------------------------------------------------------------------------------        
        [x_int, y_int, z_int]= getCoordInSystem(arg1, arg2, arg3, detector, vide, cartesian);

        if y_int>size(cathode,1) || x_int>size(cathode,2) || z_int>size(cathode,3)
            traj= 1;
            return
        end

        isOutsideDetector= isnan(interp3(x,y,z,inDetector,x_int,y_int,z_int));
        isNearlyOutsideDetector= (interp3(x,y,z,(densge~=0),x_int,y_int,z_int)~=1); % not at border
        traj= isOutsideDetector | isNearlyOutsideDetector;   % return 1 if 
    case 'end'
%-------------------------------------------------------------------------------
%- free memory at the end of a scan
%-------------------------------------------------------------------------------        
        clear global cartesian cathode anode detector vide stepTime voltage t scale notElect displayTraj
        clear global inDetector dvhx dvhy dvhz dvex dvey dvez densge
        clear global sLogo sDate geometryName element
    case 'calc'
        method= '*linear';
%-------------------------------------------------------------------------------
%- compute the trajectory from the given interaction point
%-------------------------------------------------------------------------------        
        if element % electron==1
            dvx= dvex; dvy= dvey; dvz= dvez; 
            tension = potential.*(densge~=0) + voltage.anode.*(densge==0);     
        else
            dvx= dvhx; dvy= dvhy; dvz= dvhz; 
            tension = potential.*(densge~=0) + voltage.cathode.*(densge==0);
        end
         
        epsMove= 1e-3;
        [x_int, y_int, z_int]= getCoordInSystem(arg1, arg2, arg3, detector, vide, cartesian);

        traj= [];    % resulting matrix [x y z t velx vely velz]
        tmp_v= [];
        bContinue= 1;
        curStepTime= 0;

        % compute the velocity at starting position
        vx= interp3(x,y,z,dvx, x_int,y_int,z_int);
        vy= interp3(x,y,z,dvy, x_int,y_int,z_int);
        vz= interp3(x,y,z,dvz, x_int,y_int,z_int);

        % compute all step until the particule reaches the electrode or
        % stops
        while bContinue==1,
            curStepTime= curStepTime + stepTime;
            traj= [traj; x_int y_int z_int curStepTime vx vy vz];
            x_curr= x_int;
            y_curr= y_int;
            z_curr= z_int;
%             vx_curr = vx;
%             vy_curr = vy;
%             vz_curr = vz;
            % compute velocity at current position
            vx= interp3(x,y,z,dvx,x_curr,y_curr,z_curr,method);    
            vy= interp3(x,y,z,dvy,x_curr,y_curr,z_curr,method);    
            vz= interp3(x,y,z,dvz,x_curr,y_curr,z_curr,method);
%             fprintf('%f %f %f xyz %f %f %f\n', vx, vy, vz, x_curr, y_curr, z_curr)
%             tmp_v= [tmp_v; vx vy vz];
            % compute next position
            y_int= y_curr + stepTime*vy;
            x_int= x_curr + stepTime*vx;
            z_int= z_curr + stepTime*vz;
            % ckeck that the particule has moved
            isMoving= (abs(x_curr-x_int)+abs(y_curr-y_int)+abs(z_curr-z_int))> epsMove;
            % check that the position is inside the detector
            tensionpoint= interp3(x,y,z,tension,x_int,y_int,z_int,'*linear');
            isInsideDetector= ...
                ((voltage.min) < tensionpoint) & (tensionpoint < (voltage.max));            
            isNotOutside= ceil(interp3(x,y,z,inDetector,x_int,y_int,z_int,'*linear'));
            
            bContinue = isMoving & isInsideDetector & isNotOutside;
            if size(traj,1)==350
                toto=1;
            end
             if size(traj,1)==550
                toto=1;
             end     
            if size(traj,1)==750
                toto=1;
            end    
            if size(traj,1)==1050
                toto=1;
            end           
        end
%         figure,
%         hold on, plot(tmp_v(:,1),'b')
%         hold on, plot(tmp_v(:,2),'r')
%         hold on, plot(tmp_v(:,3),'k')
%         hold off
% compute the last step where it hits the electrode
if isMoving && isNotOutside && (size(traj,1)>5)
    if element, goal = voltage.anode; else goal = voltage.cathode; end
    minStepTime = spline(interp3(x,y,z,tension,traj(:,1),traj(:,2),traj(:,3),'*linear'), traj(:,4), goal) - traj(end,4);
    if minStepTime> stepTime*1e-3
        y_int= y_curr + minStepTime*vy;
        x_int= x_curr + minStepTime*vx;
        z_int= z_curr + minStepTime*vz;
        curStepTime= curStepTime + minStepTime;
        traj= [traj; x_int y_int z_int curStepTime vx vy vz];
    end
end

if verbose
    plot_trajectory(mgsversion, densge, cathode, anode, traj, element);
end
end %switch

%===============================================================================
function [x_int, y_int, z_int]= getCoordInSystem(x, y, z, detector, vide, planar)
if planar
    y_int= y*(10/detector.step)+vide.up;
    x_int= x*(10/detector.step)+vide.left;
    z_int= z*(10/detector.step)+vide.forward;
else
    [x_int,y_int]= pol2cart(y*(pi/180),x*(10/detector.step));
    x_int= x_int+vide.left+(detector.length)/2;
    y_int= y_int+vide.up  +(detector.height)/2;
    z_int= z*(10/detector.step)+vide.forward;
end


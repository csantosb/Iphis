function res= io_existdata(handles, x,y,z, type)
%io_existscan - check if data have already been computed and saved
% Inputs
%       o handles   structure with handles and user data (see GUIDATA)
%       o x         first coordinate of the interaction point
%       o y         second coordinate
%       o z         third coordinate
%       o type      'current' or 'charge'
% Output
%       o res      '1' means file already exists 

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

datapath= [handles.datapath, handles.detector, filesep, type, filesep];
if strcmp(handles.scancoord, 'cartesian');
    filename= ['z',num2str(z),filesep,type,'_x',num2str(x),'_y',num2str(y)];
else
    filename= ['z',num2str(z),filesep,type,'_r',num2str(x),'_t',num2str(y)];
end

if strcmp(handles.scandata, 'binary')
    ext= '.bin';
else 
    ext= '.txt';
end

res= (exist([datapath, filename, ext]) == 2);

if res & strcmp(type,'current') % check that charge exist too
    datapath= [handles.datapath, handles.detector, filesep, 'charge', filesep];
    if strcmp(handles.scancoord, 'cartesian');
        filename= ['z',num2str(z),filesep,'charge','_x',num2str(x),'_y',num2str(y)];
    else
        filename= ['z',num2str(z),filesep,'charge','_r',num2str(x),'_t',num2str(y)];
    end
    res= (exist([datapath, filename, ext]) == 2);
end

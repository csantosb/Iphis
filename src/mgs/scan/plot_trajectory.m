function plot_trajectory(sData, densge, cathode, anode, traj, type)
% todo: rewrite all

figure,hold on
set(gcf,'name',sData);
[x,y,z]=meshgrid([1:size(densge,2)],[1:size(densge,1)],[1:size(densge,3)]);

plotmatrix= (cathode~=0);
p= patch(isosurface(x,y,z,plotmatrix,.1));
isonormals(x,y,z,plotmatrix,p);
isocolors(x,y,z,plotmatrix,p);
set(p,'FaceColor','blue','EdgeColor','none');

plotmatrix=(anode~=0);
p=patch(isosurface(x,y,z,plotmatrix,.9));
isonormals(x,y,z,plotmatrix,p);
isocolors(x,y,z,plotmatrix,p);
set(p,'FaceColor','yellow','EdgeColor','none');

switch (type)
    case 1% 'ELECTRON'
        plot3(traj(:,1),traj(:,2),traj(:,3),'r','linewidth',2);
    case 0% 'HOLE'
        plot3(traj(:,1),traj(:,2),traj(:,3),'b','linewidth',2);
    case 'BOTH'
        nbStep= find(traj(:,1)>0);
        holeTraj= traj(nbStep,1:3);
        nbStep= find(traj(:,4)>0);
        electronTraj= traj(nbStep,4:6);
        plot3(holeTraj(:,1),holeTraj(:,2),holeTraj(:,3),'b','linewidth',2);
        plot3(electronTraj(:,1),electronTraj(:,2),electronTraj(:,3),'r','linewidth',2);
    otherwise
        fprintf('Plot_trajectory: option not implemented\n');
end

alpha(.2);
view(3);
set(gca,'DataAspectRatio',[1 1 1]);
camlight;
lighting phong;


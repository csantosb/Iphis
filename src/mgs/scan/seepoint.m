function seepoint(handles)

if handles.detector(1)=='-',
    fprintf('Please select a valid library\n'),return
end

datapath= [handles.datapath,handles.detector,filesep];
load([datapath,'geometry.mat']);
load([datapath,'potential_mapping.mat'],'potential');
load([datapath,'drift_velocities.mat']);
if sum(densge(:))>0
    voltage=(potential<=0);             % type n detector, q > 0
else
    voltage=(potential>=0);             % type p detector, q < 0
end

% number of segments (including the ring)
nbSegmentCathode= double(max(cathode(:)));
nbSegmentAnode= double(max(anode(:)));
nbSegment= nbSegmentCathode + nbSegmentAnode;

if strcmp(handles.scancoord,'cartesian'),
    prompt={'x','y','z'};
    def={int2str(round(dimension.nwcols/2)),int2str(round(dimension.nrows/2)),int2str(round(dimension.nprofs/2))};
else
    prompt={'r','t','z'};
    def={int2str(round(detector.length/2)),int2str(45),int2str(round(dimension.nprofs/2))};
end
answer=inputdlg(prompt,'Enter the point to plot ...',1,def);
if isempty(answer),return,end


% format of the data
currentFormat=[' %8.3e %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f '];
for i=1:(nbSegmentCathode+nbSegmentAnode),
    currentFormat=[currentFormat,' %10.5e  %10.5e '];
end
currentFormat=[currentFormat,' \n'];

chargeFormat= ['%8.3e ']; % time
for i=1:(nbSegmentCathode+nbSegmentAnode),
    chargeFormat=[chargeFormat,' %10.5e '];
end
chargeFormat= [chargeFormat,' \n'];

charge= io_loaddata(handles, answer{1}, answer{2}, answer{3}, chargeFormat , nbSegment, 'charge');
if isempty(charge),return,end
current= io_loaddata(handles, answer{1}, answer{2}, answer{3}, currentFormat , nbSegment, 'current');

if ~isempty(current)
    [traj_ho, traj_el, pulse_holes, pulse_electrons, pulse_total, timevector]= ...
        getdata(current, nbSegment);
    %-------------------------------------------------------------------------------
    %- plot trajectories
    %-------------------------------------------------------------------------------
    figure,hold on
    set(gcf,'name',[handles.version,handles.releasedate]);
    [x,y,z]=meshgrid([1:dimension.nwcols],[1:dimension.nrows],[1:dimension.nprofs]);

    plotmatrix= (cathode~=0);
    p= patch(isosurface(x,y,z,plotmatrix,.1));
    isonormals(x,y,z,plotmatrix,p);
    isocolors(x,y,z,plotmatrix,p);
    set(p,'FaceColor','blue','EdgeColor','none');

    plotmatrix=(anode~=0);
    p=patch(isosurface(x,y,z,plotmatrix,.9));
    isonormals(x,y,z,plotmatrix,p);
    isocolors(x,y,z,plotmatrix,p);
    set(p,'FaceColor','yellow','EdgeColor','none');

    reduc=8;
    x_=vide.left+1:reduc:vide.left+detector.length;
    y_=vide.up+1:reduc:vide.up+detector.height;
    z_=vide.forward+1:reduc:vide.forward+detector.depth;
    [u,v,w]=meshgrid(x_,y_,z_);

    Vx=dvh.x(y_,x_,z_).*(densge(y_,x_,z_)~=0);
    Vy=dvh.y(y_,x_,z_).*(densge(y_,x_,z_)~=0);
    Vz=dvh.z(y_,x_,z_).*(densge(y_,x_,z_)~=0);

    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    plot3(traj_ho(:,1),traj_ho(:,2),traj_ho(:,3),'k','linewidth',5)

    % Display
    fprintf('[Plot trajectory] Last point of h+: %3.2f %3.2f %3.2f \n', ...
        traj_ho(end,1), traj_ho(end,2), traj_ho(end,3));

    % trajectory of e-
    Vx=dve.x(y_,x_,z_).*(densge(y_,x_,z_)~=0);
    Vy=dve.y(y_,x_,z_).*(densge(y_,x_,z_)~=0);
    Vz=dve.z(y_,x_,z_).*(densge(y_,x_,z_)~=0);

    qh=quiver3(u,v,w,Vx,Vy,Vz);
    set(qh,'Color','green')

    axis tight;
    %    grid minor;
    alpha(.1);
    view(3);
    set(gca,'DataAspectRatio',[1 1 1]);
    camlight;
    lighting phong;

    plot3(traj_el(:,1),traj_el(:,2),traj_el(:,3),'r','linewidth',5)

    xlabel(['interaction point x=',num2str(traj_el(1,1)-vide.left),' y=',num2str(traj_el(1,2)-vide.up),' z=',num2str(traj_el(1,3)-vide.forward)])
    title(['Trajectory for charge carriers' ])

    % Display
    fprintf('[Plot trajectory] Last point of e-: %3.2f %3.2f %3.2f \n', ...
        traj_el(end,1), traj_el(end,2), traj_el(end,3));

    %-------------------------------------------------------------------------------
    %- plot currents
    %-------------------------------------------------------------------------------
    a=round(sqrt(nbSegmentCathode));
    b=ceil(nbSegmentCathode/a);
    % Current at the cathode
    figure,hold on,set(gcf,'name','  Cathode: Current h+(b),e-(r),total(k) / time(s)')
    for i=1:nbSegmentCathode
        subplot(a,b,i)
        plot(timevector,pulse_total    (:,i)','k',...
            timevector,pulse_holes    (:,i)','b',...
            timevector,pulse_electrons(:,i)','r'),%grid
    end

    a=round(sqrt(nbSegmentAnode));
    b=ceil(nbSegmentAnode/a);
    % Current at the anode
    figure,hold on,set(gcf,'name','  Anode: Current h+(b),e-(r),total(k) / time(s)')
    for i=1:nbSegmentAnode
        subplot(a,b,i)
        plot(timevector,pulse_total    (:,nbSegmentCathode+i)','k',...
            timevector,pulse_holes    (:,nbSegmentCathode+i)','b',...
            timevector,pulse_electrons(:,nbSegmentCathode+i)','r'),%grid
    end
end

%-------------------------------------------------------------------------------
%- plot charge
%-------------------------------------------------------------------------------
timevector= charge(:, 1);
charge= charge(:, 2:end);

a=round(sqrt(nbSegmentCathode));
b=ceil(nbSegmentCathode/a);
chargecathode=charge(:,1:nbSegmentCathode);
[d1,d2]=find(abs(chargecathode)==max(abs(chargecathode(:))));
chargecathode(:,d2)=zeros(size(chargecathode(:,d2)));
Maximo=max(chargecathode(:));
Minimo=min(chargecathode(:));
figure,hold on,set(gcf,'name','  Cathode: Charge (numeric) / time(s)')
for i=1:nbSegmentCathode
    subplot(a,b,i)
    plot(timevector,charge(:,i)','r'),zoom on
    if i~=d2,axis([timevector(1) timevector(end) Minimo Maximo]),end
end

a=round(sqrt(nbSegmentAnode));
b=ceil(nbSegmentAnode/a);
figure,hold on,set(gcf,'name','  Anode: Charge (numeric) / time(s)')
for i=1:nbSegmentAnode
    subplot(a,b,i)
    plot(timevector,charge(:,nbSegmentCathode+i)','r'),grid
end

function [traj_ho, traj_el, pulse_holes, pulse_electrons, pulse_total, timevector]= ...
    getdata(data, nbsegment)

% trajectory of h+
traj_ho=data(:,2:4);
traj_ho=sum(traj_ho,2);
index=find(traj_ho);
traj_ho=data(index,2:4);
% trajectory of e-
traj_el=data(:,5:7);
traj_el=sum(traj_el,2);
index=find(traj_el);
traj_el=data(index,5:7);
% current of h+
pulse_holes=data(:,8:(8+nbsegment-1));
% current of e-
pulse_electrons=data(:,(8+nbsegment):(8+2*nbsegment-1));
% total current
pulse_total=pulse_holes+pulse_electrons;
% timevector
timevector= data(:,1);

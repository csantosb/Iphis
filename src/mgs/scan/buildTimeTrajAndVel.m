%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build new time vector and accurate trajectories and velocities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [electronCharge, holeCharge, electronVel, holeVel, timeVector]= buildTimeTrajAndVel(traj_el, traj_ho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build time vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if size(traj_el,1)>= size(traj_ho,1)
    specialTimeStep= traj_ho(end,4);
    if isempty(find(traj_el(:,4)== specialTimeStep))
        timeVector= sort([traj_el(:,4); specialTimeStep]);
    else
        timeVector= traj_el(:,4);
    end
else
    specialTimeStep= traj_el(end,4);
    if isempty(find(traj_ho(:,4)== specialTimeStep))
        timeVector= sort([traj_ho(:,4); specialTimeStep]);
    else
        timeVector= traj_ho(:,4);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% retime trajectories and velocities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prepare data
nbSteps= length(timeVector);
tmpVector= zeros(nbSteps,1); % check
oldTimeVector= traj_ho(:,4);
holeTraj= zeros(nbSteps,7,'single');
% compute
for i= [1 2 3 5 6 7] % each coordinate for traj and vel
    if size(traj_ho,1)>1
        tmpVector= interp1(oldTimeVector,traj_ho(:,i), timeVector);
        tmpNbValue= find(~isnan(tmpVector));
        holeTraj(1:length(tmpNbValue),i)= tmpVector(tmpNbValue);
    else
        holeTraj(1,i)= traj_ho(:,i);
    end
end % i= 1:7
holeTraj(:,4)= [];
holeLength= length(find(holeTraj(:,1)>0));

% prepare data
oldTimeVector= traj_el(:,4);
electronTraj= zeros(nbSteps,7,'single');
% compute
for i= [1 2 3 5 6 7] % each coordinate for traj and vel
    if size(traj_el,1)>1
        tmpVector= interp1(oldTimeVector,traj_el(:,i), timeVector);
        tmpNbValue= find(~isnan(tmpVector));
        electronTraj(1:length(tmpNbValue),i)= tmpVector(tmpNbValue);
    else
        electronTraj(1,i)= traj_el(:,i);
    end
end % i= 1:7
electronTraj(:,4)= [];
electronLength= length(find(electronTraj(:,1)>0));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build charge trajectories and velocities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
electronCharge= electronTraj(1:electronLength,1:3);
holeCharge= holeTraj(1:holeLength,1:3);
electronVel= electronTraj(1:electronLength,4:6); 
holeVel= holeTraj(1:holeLength,4:6);
end

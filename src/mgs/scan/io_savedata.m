function io_savedata(handles, x,y,z, dataformat, data, type)
%io_savecharge - save all pulses on one file
% Inputs
%       o handles   structure with handles and user data (see GUIDATA)
%       o x         first coordinate of the interaction point
%       o y         second coordinate
%       o z         third coordinate
%       o dataformat  description of the data format
%       o type      'current' or 'charge'
% Results
%       o file      containing the pulses for each segments  

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

datapath= [handles.datapath, handles.detector, filesep, type, filesep];
if strcmp(handles.scancoord, 'cartesian')
    filename= ['z',num2str(z),filesep,type,'_x',num2str(x),'_y',num2str(y)];
else
    filename= ['z',num2str(z),filesep,type,'_r',num2str(x),'_t',num2str(y)];
end

if strcmp(handles.scandata, 'binary')
    fid= fopen([datapath, filename, '.bin'],'w');
    fwrite(fid,data,'single');
else    % ascii
    fid= fopen([datapath, filename, '.txt'],'w');
    fprintf(fid,dataformat,data');
end
fclose(fid);


function scan(handles, IPCoord, mode)
% scan scan a list of points to produce trajectories charges and current
%    for each segment of the selected detector
% Inputs
%       o handles   structure with handles and user data (see GUIDATA)
%       oIPCoord	list of all the interaction points to compute (in cartesian or
%                   cylindrical coordinates)
%       o mode      'single', 'gui' or 'batch' according to the way this file is
%                   launched
% Results
%       o in mode 'gui' or 'batch', save the trajectories, charges and current
%       o in mode 'button', show the data and save it in "LastPulseShape.txt"

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

% Todo : neutron damage

INTERPMETHOD= '*linear';       % to compute the current from the weighting potential

startdate= datestr(now);

%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
% constants
ELECTRON= 1;        % used in computetrajectory
HOLE= 0;
% load data
datapath= [handles.datapath, handles.detector, filesep];
load([datapath,'geometry.mat'], 'dimension', 'densge', 'cathode', 'anode', 'detector');

% prepare statistics
nbPoint= size(IPCoord,1);
packSize= handles.packsize;
prevPackSize= packSize;
nbPack= floor(nbPoint/packSize); % pack goes from 0 to nbPack --> nbPack+1 to do
nbPointActuallyComputed= 0;
if nbPoint>=1  % not a single point (gui mode)
    if mod(nbPoint,packSize)==0,      nbPack= nbPack-1; end
    fprintf('[Scan] Number of points to scan: %d, number of packs: %d, pack size: %d\n', ...
        nbPoint, nbPack+1, packSize);
end

% initialisation
if ~strcmp(mode, 'single') handles.plotenable= 1; end;
% trajectories
computetrajectory('init', handles);
% number of segments (including the ring)
nbSegmentCathode= single(max(cathode(:)));
nbSegmentAnode= single(max(anode(:)));
[x,y,z]= meshgrid([1:dimension.nwcols],[1:dimension.nrows],[1:dimension.nprofs]);
w =2.96;            % from Martina
e=1.60217733e-19;   % charge of the electron (Coulomb)
edeposit= 1e6;      % energy deposit (eV)
C= 1e12;
ew= e*C*(edeposit/w); % to be applied to charge (electronPulse, holePulse)
neutronDamage= 0; % strcmp(handles.neutron_damage_on,'on');
% format of the data
currentFormat=[' %8.3e %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f '];
for i=1:(nbSegmentCathode+nbSegmentAnode),
    currentFormat=[currentFormat,' %10.5e  %10.5e '];
end
currentFormat=[currentFormat,' \n'];

chargeFormat= ['%8.3e ']; % time
for i=1:(nbSegmentCathode+nbSegmentAnode),
    chargeFormat=[chargeFormat,' %10.5e '];
end
chargeFormat= [chargeFormat,' \n'];

% save data
if ~strcmp(mode, 'single')
    io_initscan(handles, IPCoord(:,3), handles.scanmode);
end

%-------------------------------------------------------------------------------
%- compute one pack of points at a time
%-------------------------------------------------------------------------------
for pack= 0:nbPack
    % check pack size
    if pack== nbPack  % change pack size
        packSize= nbPoint- (nbPack*packSize);
    end
    % initialize pack data for trajectories
    packNbPoint= 0;                          % number of IP actually computed in the current pack
    packCoord= zeros(packSize,3,'single');   % to save all the IP coordinates computed in the current pack
    packDataElSize= 0; packDataEl= single([0 0 0]);
    packDataHoSize= 0; packDataHo= single([0 0 0]);

    packDataSegments= zeros(1, 1+6+(nbSegmentCathode+nbSegmentAnode)*2, 1, 'single');
    packDataVel= zeros(1, 6, 1, 'single');  % for velocities (hole electron)
    packDataSegmentsSize= 0;
    %-------------------------------------------------------------------------------
    %- compute trajectories
    %-------------------------------------------------------------------------------
    for i=1:packSize
        xip= IPCoord(pack*prevPackSize+i,1);
        yip= IPCoord(pack*prevPackSize+i,2);
        zip= IPCoord(pack*prevPackSize+i,3);
        % check if the point has to be computed
        if ~strcmp(mode, 'single') & io_existdata(handles, xip,yip,zip, handles.scanmode)
            fprintf('[%3.1f %3.1f %3.1f] already performed\n',xip,yip,zip);
        elseif computetrajectory('outside', xip, yip, zip, 0)
            % check if point is outside the detector
            fprintf('[%3.1f %3.1f %3.1f] too close to an electrode or outside of germanium\n',...
                xip,yip,zip);
        else
            nbPointActuallyComputed= nbPointActuallyComputed+1;
            fprintf('[%3.1f %3.1f %3.1f] computing (%d/%d)\n',xip,yip,zip, pack*prevPackSize+i, nbPoint);
            traj_el= computetrajectory('calc', xip, yip, zip, 0, ELECTRON);
            traj_ho= computetrajectory('calc', xip, yip, zip, 0, HOLE);

            % compute neutron damage
            %             if neutronDamage
            %                dist_h= traj_ho - [traj_ho(1,:);traj_ho(1:end-1,:)];
            %                dist_h = sqrt((sum(dist_h .* dist_h,2)))/(10/detector.step);
            %                dist_h = cumsum(dist_h);
            %             end

            % check that trajectory exists
            if (size(traj_ho,1)<= 2 | size(traj_el,1)<= 2)
                fprintf('[%3.1f %3.1f %3.1f] no trajectories computed (too close to an electrode)\n',xip,yip,zip);
            else
                % compute charge, velocity and time steps
                [electronTraj, holeTraj, electronVel, holeVel, timeVector]= buildTimeTrajAndVel(traj_el, traj_ho);
                nbStep= length(timeVector);
                nbRealStepEl= size(electronTraj,1);
                nbRealStepHo= size(holeTraj,1);

                % save trajectory and interaction point and nbPointInPack
                packNbPoint= packNbPoint+1;
                packCoord(packNbPoint,:)= [xip yip zip];
                if nbStep > packDataSegmentsSize
                    packDataSegments(packDataSegmentsSize+1: nbStep,:,:)= 0;
                    packDataSegmentsSize= nbStep;
                end

                nbRealStepEl= size(electronTraj,1);
                nbRealStepHo= size(holeTraj,1);

                packDataSegments(1:nbStep, 1, packNbPoint)= timeVector;
                packDataSegments(1:nbRealStepHo, 2:4, packNbPoint)= holeTraj;
                packDataSegments(1:nbRealStepEl, 5:7, packNbPoint)= electronTraj;
                packDataVel(1:nbRealStepHo, 1:3, packNbPoint)= holeVel.*0.1;
                packDataVel(1:nbRealStepEl, 4:6, packNbPoint)= electronVel.*0.1;

                % TODO save neutron damage ( format: exp(-dist_h/1e3) at
                % the end of packDataVel (if necessary)
            end
        end % io_existScan / trajectory_electron / ...
    end % i=1:packSize

    if packNbPoint>0
        %-------------------------------------------------------------------------------
        %- compute charge on cathode
        %-------------------------------------------------------------------------------
        for segment=1:nbSegmentCathode
            % load weighting potential
            load([datapath,'weightingpotential_cathode_pixel',num2str(segment),'.mat'],'potential')

            for i=1:packNbPoint
                % rebuild data for current point
                timeVector= packDataSegments(:,1,i);
                timeVector= timeVector(find(timeVector>0));
                nbRealStepHo= find(packDataSegments(:, 2, i)>0);
                holeTraj= packDataSegments(nbRealStepHo, 2:4, i);
                holeVel= packDataVel(nbRealStepHo, 1:3, i);

                nbRealStepEl= find(packDataSegments(:, 5, i)>0);
                electronTraj= packDataSegments(nbRealStepEl, 5:7, i);
                electronVel= packDataVel(nbRealStepEl, 4:6, i);
                %-------------------------------------------------------------------------------
                %- compute charge from electrons and holes trajectories
                %-------------------------------------------------------------------------------
                electronPulse= ew*interp3(x,y,z,potential,electronTraj(nbRealStepEl,1), ...
                    electronTraj(nbRealStepEl,2), electronTraj(nbRealStepEl,3), INTERPMETHOD);
                holePulse= -ew*interp3(x,y,z,potential,holeTraj(nbRealStepHo,1), ...
                    holeTraj(nbRealStepHo,2), holeTraj(nbRealStepHo,3), INTERPMETHOD);
                %-------------------------------------------------------------------------------
                %- apply neutron damage
                %-------------------------------------------------------------------------------
                % TODO
                %-------------------------------------------------------------------------------
                %- save data (HolePulse then ElectronPulse), and chargeTraj (in
                %- packData)
                %-------------------------------------------------------------------------------
                packDataSegments(nbRealStepHo, 7+segment, i)= holePulse;
                packDataSegments(nbRealStepHo(end):end, 7+segment, i)= holePulse(end);
                packDataSegments(nbRealStepEl, 7+(nbSegmentCathode+nbSegmentAnode)+segment, i)= electronPulse;
                packDataSegments(nbRealStepEl(end):end, 7+(nbSegmentCathode+nbSegmentAnode)+segment, i)= electronPulse(end);
            end % for i=1:nbPointInPack
        end % for segment=1:nbSegmentCathode

        %-------------------------------------------------------------------------------
        %- compute charge on anode (same code as cathode)
        %-------------------------------------------------------------------------------
        for segment=1:nbSegmentAnode
            load([datapath,'weightingpotential_anode_pixel',num2str(segment),'.mat'],'potential')

            for i=1:packNbPoint
                % rebuild data for current point
                timeVector= packDataSegments(:,1,i);
                timeVector= timeVector(find(timeVector>0));
                nbRealStepHo= find(packDataSegments(:, 2, i)>0);
                holeTraj= packDataSegments(nbRealStepHo, 2:4, i);
                holeVel= packDataVel(nbRealStepHo, 1:3, i);

                nbRealStepEl= find(packDataSegments(:, 5, i)>0);
                electronTraj= packDataSegments(nbRealStepEl, 5:7, i);
                electronVel= packDataVel(nbRealStepEl, 4:6, i);

                %-------------------------------------------------------------------------------
                %- compute charge from electrons and holes trajectories
                %-------------------------------------------------------------------------------
                holePulse= -ew*interp3(x,y,z,potential,holeTraj(nbRealStepHo,1), ...
                    holeTraj(nbRealStepHo,2), holeTraj(nbRealStepHo,3), INTERPMETHOD);
                electronPulse= ew*interp3(x,y,z,potential,electronTraj(nbRealStepEl,1), ...
                    electronTraj(nbRealStepEl,2), electronTraj(nbRealStepEl,3),INTERPMETHOD);
                %-------------------------------------------------------------------------------
                %- apply neutron damage
                %-------------------------------------------------------------------------------
                % TODO
                %-------------------------------------------------------------------------------
                %- save data (HolePulse then ElectronPulse), and chargeTraj (in
                %- packData)
                %-------------------------------------------------------------------------------
                packDataSegments(nbRealStepHo, 7+nbSegmentCathode+segment, i)= holePulse;
                packDataSegments(nbRealStepHo(end):end, 7+nbSegmentCathode+segment, i)= holePulse(end);
                packDataSegments(nbRealStepEl, 7+(2*nbSegmentCathode+nbSegmentAnode)+segment, i)= electronPulse;
                packDataSegments(nbRealStepEl(end):end, 7+(2*nbSegmentCathode+nbSegmentAnode)+segment, i)= electronPulse(end);
                
%                 % temp save data in mat file
%                 if segment==1
%                 save(['efieldnormaltraj', num2str(i), '.mat'], 'timeVector', 'holeTraj', 'holeVel', ...
%                     'electronTraj', 'electronVel', 'nbRealStepHo', 'nbRealStepEl');
%                 end
%                 %temp
            end % for i=1:nbPointInPack
        end % for i=1:nbSegmentAnode

        %-------------------------------------------------------------------------------
        %- save data on file
        %-------------------------------------------------------------------------------
        for point= 1:packNbPoint
            xp= packCoord(point,1); yp= packCoord(point,2); zp=packCoord(point,3);
            nbStep= find(packDataSegments(:, 1, point)>0);
            traj= packDataSegments(nbStep, 1:7, point);
            timeVector= traj(:,1); traj(:,1)= [];
            nbRealStepHo= length(find(traj(:, 1)>0));
            nbRealStepEl= length(find(traj(:, 4)>0));
            % retrieve pulse
            holePulse= packDataSegments(nbStep, 8:7+nbSegmentCathode+nbSegmentAnode, point);
            electronPulse= packDataSegments(nbStep, 7+nbSegmentCathode+nbSegmentAnode+1:end, point);
            totalPulse= holePulse+ electronPulse;
            if strcmp(handles.scanmode, 'current')
                % compute current
                holeCurrent = ComputeCurrent(holePulse, timeVector, nbSegmentCathode+nbSegmentAnode, nbRealStepHo);
                electronCurrent = ComputeCurrent(electronPulse, timeVector, nbSegmentCathode+nbSegmentAnode, nbRealStepEl);
            else
                holeCurrent= zeros(size(holePulse));
                electronCurrent= zeros(size(electronPulse));
            end
            if strcmp(mode, 'single')
                plot_trajectory([handles.version, handles.releasedate], densge, cathode, anode, traj, 'BOTH');
                plot_charge(datapath, packCoord, [timeVector traj holeCurrent electronCurrent holePulse electronPulse], currentFormat, nbSegmentCathode, nbSegmentAnode);
            else
                if strcmp(handles.scanmode, 'current')
                io_savedata(handles, xp, yp, zp, currentFormat, [timeVector traj holeCurrent electronCurrent], 'current');
                end
                io_savedata(handles, xp, yp, zp, chargeFormat, [timeVector, totalPulse], 'charge');
            end
            
%             % temp
%             save(['efieldnormalpulse', num2str(point), '.mat'], 'holePulse', 'electronPulse', 'holeCurrent',...
%                 'electronCurrent', 'traj');
%             % end temp
        end
    end % packNbPoint>0
end
computetrajectory('end');
enddate= datestr(now);
fprintf('[%s] %d points computed\t[%s]\n', startdate, nbPointActuallyComputed, enddate);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function current= ComputeCurrent_csb(charge, timeVector, nbSegment, nbRealStep)
%     current = zeros(size(charge));
%     stp = 10;
%     vector = [1:stp:nbRealStep];
%     vector = [vector,vector(end)+1:nbRealStep];
%     chargeInterp = spline(timeVector(vector),charge(vector,:)',timeVector)';
%     [fx,fy] = gradient(chargeInterp([1:nbRealStep],:));
%     tiempo = repmat(gradient(timeVector([1:nbRealStep])),1,nbSegment);
%     current(1:nbRealStep,:) = 1e-12*fy./tiempo;
%     if size(charge,1)> nbRealStep
%         current(nbRealStep,:)= current(nbRealStep-1,:);
%         current(nbRealStep+1:end,:)= 0;
%     else
%         current(end,:)= current(end-1,:);
%     end

function current= ComputeCurrent_csb(charge, timeVector, nbSegment, nbRealStep)
current = zeros(size(charge));
stp = 10;
%     vector = [5:stp:nbRealStep];
%     vector = [vector,vector(end)+1:nbRealStep];
%     vector = [vector,nbRealStep];
vector = [nbRealStep:-stp:1];
if vector(end)~=1,vector = [vector,1];end
vector = flipdim(vector,2);

for i=1:nbSegment
    chargeInterp = spline(timeVector(vector),charge(vector,i),timeVector);
    %         figure,plot(timeVector([1:nbRealStep]),charge(1:nbRealStep,i),'o',timeVector([1:nbRealStep]),chargeInterp([1:nbRealStep]),'-');
    current(1:nbRealStep,i) = 1e-12*gradient(chargeInterp([1:nbRealStep]))./gradient(timeVector([1:nbRealStep]));
    %         figure,plot(timeVector,current(:,i),'*')
    %         charge2 = cumtrapz(holeCurrent);
    %         figure,plot(timeVector,charge,'*')
end
if size(charge,1)> nbRealStep
    current(nbRealStep,:)= current(nbRealStep-1,:);
    current(nbRealStep+1:end,:)= 0;
else
    current(end,:)= current(end-1,:);
end
charge2 = ComputeCharge(current, timeVector, nbSegment, 1e12);

function charge= ComputeCharge(current, timeVector, nbSegment, capacity)
charge= [];
for i=1:nbSegment
    y= current(:,i)';
    q= trapez(timeVector',y);
    charge(:,i)= capacity*q';
end

function q= trapez(x,y)
ly = length(y);
q = ( [0 y] + [y 0] ) / 2 .* ( [x 0] - [0 x] );
q =  cumsum(q(1:end-1));

function current= ComputeCurrent(charge, timeVector, nbSegment, nbRealStep)
C= 1e-12;
current= zeros(size(charge));
realStep= [1:nbRealStep];
for i=1:nbSegment
    current([2:nbRealStep],i)= C.*diff(charge(realStep,i))./diff(timeVector(realStep));
end
if size(charge,1)> nbRealStep
    current(nbRealStep,:)= current(nbRealStep-1,:);
    current(nbRealStep+1:end,:)= 0;
else
    current(end,:)= current(end-1,:);
end



function wPotential= ComputeWPotential(tension, traj, timeVector, realStep)
if 1 % new calculus
    wPotential= interp3(tension, traj(:,1), traj(:,2), traj(:,3));
    wPotential= smooth(wPotential, 35, 'loess');%smooth(timeVector(realStep), wPotential, 15);
    % smooth
else % previous calculus
    wPotential= interp3(tension, traj(:,1), traj(:,2), traj(:,3), '*cubic'); % test cubic, spline
    notNanValue= find(~isnan(wPotential));
    if length(notNanValue) < 2
        wPotential= zeros(size(wPotential));
    else
        % find if last value is zero or 1
        wPotentialnoNan= wPotential(notNanValue);
        if (wPotentialnoNan(end))<0.5
            wPotential(end)= 0;
        else
            wPotential(end)= 1;
        end
        notNanValue= [notNanValue; length(wPotential)];
        if isnan(wPotential(1))
            notNanValue= [1; notNanValue];
            if (wPotentialnoNan(1))<0.5
                wPotential(1)= 0;
            else
                wPotential(1)= 1;
            end
        end
        wPotential= interp1(timeVector(notNanValue), wPotential(notNanValue), timeVector(nbRealStep), 'pchip');
    end
end

function wPotential= ComputeSmoothWPotential(tension, traj, timeVector, nbRealStep, nbRealStepOther)
keepStep= nbRealStep;
if (timeVector(end)-timeVector(end-1)) ~= timeVector(1)
    keepStep(nbRealStep(end))= [];
end
if length(nbRealStep)>nbRealStepOther
    % may have to remove last step other
    if (nbRealStepOther(end)-nbRealStepOther(end-1)) ~= timeVector(1)
        keepStep(nbRealStepOther(end))= [];
    end
end

wPotential= zeros(size(nbRealStep));

wPotential(keepStep)= interp3(tension, traj(keepStep,1), traj(keepStep,2), traj(keepStep,3));
wPotential(keepStep)= smooth(wPotential(keepStep), 30, 'loess');
wPotential= interp1(timeVector(keepStep), wPotential(keepStep), timeVector(nbRealStep),'cubic');



% the last value may be negative -> 0
% wPotential(find(wPotential<0))= 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DATAFORMAT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2D packCoord-> [x y z outOfVolumeHo outOfVolumeEl] (nbPoint, 5)
% 3D packDataEl-> [xtraj, ytraj, ztraj, t, xvel, yvel, zvel] (nbSteps,7,nbPoints)
% 3D packDataHo-> [t xtraj, ytraj, ztraj, t, xvel, yvel, zvel, neutron] (nbSteps,7+1,nbPoints)
% 3D packDataSegments-> [t ho_[xtraj, ytraj, ztraj] el_[xtraj, ytraj,
% ztraj] ho_ca[1..nb_ca] ho_an[1..nb_an] el_ca[1..nb_ca] el_an[1..nb_an]]
% (nbSteps, 1+7+(nbSegment*2))
% from _trajectory  [xtraj, ytraj, ztraj, t, xvel, yvel, zvel] (nbSteps,7)

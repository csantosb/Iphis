function io_initscan(handles, zfolder, type)
%io_initscan - create all necessary folder according to the list of points
% Inputs
%       o handles   structure with handles and user data (see GUIDATA)
%       o zfolder   contains all z coordinates from the list of interaction
%                   points

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

datapath= [handles.datapath, handles.detector, filesep, 'charge', filesep];
currentpath= [handles.datapath, handles.detector, filesep, 'current', filesep];
buildCurrentFolder= strcmp(type, 'current');
for z= 1:length(zfolder)
    if ~(exist([datapath,'z',num2str(zfolder(z))])==7),
        eval(['!mkdir "',datapath,'z',num2str(zfolder(z)),'"']),
    end
    if buildCurrentFolder & ~(exist([currentpath,'z',num2str(zfolder(z))])==7),
        eval(['!mkdir "',currentpath,'z',num2str(zfolder(z)),'"']),
    end
end

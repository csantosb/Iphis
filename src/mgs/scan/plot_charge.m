function scan_plot_charge(path, packCoord, data, dataFormat, nbSegmentCathode, nbSegmentAnode)
% todo rewrite all

% retrieve data
xp= packCoord(1,1); yp= packCoord(1,2); zp=packCoord(1,3);
nbSegment= nbSegmentCathode+nbSegmentAnode;
timeVector= data(:,1);                      data(:,1)= [];
traj= data(:, 1:6);                         data(:, 1:6)= [];
holeCurrent= data(:, 1:nbSegment);          data(:, 1:nbSegment)= [];
electronCurrent= data(:, 1:nbSegment);      data(:, 1:nbSegment)= [];
holeCharge= data(:, 1:nbSegment);           data(:, 1:nbSegment)= [];
electronCharge= data(:, 1:nbSegment);       ;

totalCurrent= holeCurrent+ electronCurrent;
totalCharge= holeCharge+ electronCharge;

% save
fid = fopen([path,'LastPulseShape.txt'],'w');
fprintf(fid, dataFormat,[timeVector traj holeCurrent electronCurrent]');
fclose(fid);
fprintf('Data saved in "LastPulseShape.txt" \n')

charge_total_dum= totalCharge(:,1:nbSegmentCathode);
[d1,d2]= find(abs(charge_total_dum)==max(abs(charge_total_dum(:))));
charge_total_dum(:,d2)= zeros(size(charge_total_dum(:,d2)));
Maximo= max(charge_total_dum(:));
Minimo= min(charge_total_dum(:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute current
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% i - Cathode
nbSegmentCathode= single(nbSegmentCathode);
a=round(sqrt(nbSegmentCathode));
b=ceil(nbSegmentCathode/a);

% Charge at the cathode
figure,hold on,set(gcf,'name','  Cathode: Charge h+(b),e-(r),total(k) / time(s)')
for i=1:nbSegmentCathode
    subplot(a,b,i, 'align')
    plot(timeVector,electronCharge     (:,i)','r',...
        timeVector, holeCharge   (:,i)','b',...
        timeVector, totalCharge(:,i)','k'),grid
end
k= 1;
% Current at the cathode
figure,hold on,set(gcf,'name','  Cathode: Current h+(b),e-(r),total(k) / time(s)')
for i=1:nbSegmentCathode
    subplot(a,b,i, 'align')
    plot(timeVector(k:end),electronCurrent(k:end,i)','r',...
        timeVector(k:end), holeCurrent    (k:end,i)','b',...
        timeVector(k:end), totalCurrent   (k:end,i)','k'),grid
end


% ii - Anode
nbSegmentAnode= single(nbSegmentAnode);
a= round(sqrt(nbSegmentAnode));
b= ceil(nbSegmentAnode/a);

% Charge at the anode
figure,hold on,set(gcf,'name','  Anode: Charge h+(b),e-(r),total(k) / time(s)')
for i=1:nbSegmentAnode
    subplot(a,b,i, 'align')
    plot(timeVector,electronCharge(:,nbSegmentCathode+i)','r',...
        timeVector, holeCharge    (:,nbSegmentCathode+i)','b',...
        timeVector, totalCharge   (:,nbSegmentCathode+i)','k'),grid
end

% Current at the anode
figure,hold on,set(gcf,'name','  Anode: Current h+(b),e-(r),total(k) / time(s)')
for i=1:nbSegmentAnode
    subplot(a,b,i, 'align')
    plot(timeVector(k:end),electronCurrent  (k:end,nbSegmentCathode+i)','r',...
        timeVector(k:end), holeCurrent    (k:end,nbSegmentCathode+i)','b',...
        timeVector(k:end), totalCurrent(k:end,nbSegmentCathode+i)','k'),grid
end

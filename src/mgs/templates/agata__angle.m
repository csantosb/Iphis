function agata__angle(handles, color)
%agata__angle - creation routine for agata detectors
%       o handles    structure with handles and user data (see GUIDATA)
%       o color      name of the detector, used to load the file containing
%                    the vertex coordinates
%   Vertex coordinates are from Enrico Farnea and can be found on the ELOG
%   forum (see ??, date may 2005)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

msg= 'Modify default value(s) ... ?';

%-------------------------------------------------------------------------------
%- grid size
%-------------------------------------------------------------------------------
answer= inputdlg({'Grid size, in mm:'}, msg, 1, {num2str(1)});
if isempty(answer), return, end
detector.step= 10*eval(answer{1});  % number of steps/0.1mm (1mm == 10 *0.1mm)
detector.length= 100*10/(detector.step);
detector.height= 100*10/(detector.step);

%-------------------------------------------------------------------------------
%- voltage and impurities
%-------------------------------------------------------------------------------
prompt={'Inner contact voltage, in volts:','Outer contact voltage, in volts:',...
    'Impurity concentration on frontend, cm-3:','Impurity concentration on backend, cm-3:','Temperature, in K'};
def= {num2str(5000),num2str(0),num2str(0.43e10),num2str(1.5e10),num2str(78)};
answer= inputdlg(prompt, msg, 1, def);
if isempty(answer),return,end
% Voltage
voltage.anode= eval(answer{1});
voltage.cathode= eval(answer{2});
% Density of Impurities in Ge
impurities.low= eval(answer{3})*(1e-6)*(detector.step^3);    % Impurities  cm3 ----> 1/10 mm3
impurities.high= eval(answer{4})*(1e-6)*(detector.step^3);
impurities.material= 'germanium';
% Temperature
temperature= eval(answer{5});

%-------------------------------------------------------------------------------
%- surrounding space
%-------------------------------------------------------------------------------
prompt= {'Forward surrounding space, in mm:','Back surrounding space, in mm:',...
    'Lateral surrounding space, in mm:'};
answer= inputdlg(prompt, msg, 1, {num2str(2), num2str(10), num2str(2)});
if isempty(answer),return,end
vide.forward= eval(answer{1}) *10/(detector.step);
vide.back   = eval(answer{2}) *10/(detector.step);
vide.up     = eval(answer{3}) *10/(detector.step);
vide.down   = eval(answer{3}) *10/(detector.step);
vide.right  = eval(answer{3}) *10/(detector.step);
vide.left   = eval(answer{3}) *10/(detector.step);

%-------------------------------------------------------------------------------
%- load agata_* containing the vertex coordinates
%-------------------------------------------------------------------------------
vertex= load([handles.parameterpath, 'agata_', color, '.txt']);
if ~isa(vertex,'double') | size(vertex)~=[12 3],
    fprintf('[error] not a valid file format\n\n')
    return
end

% [x,y,z] front face coordinates in mm (centered at [0,0])
P1= vertex(1:6,:)*10/(detector.step);
% [x,y,z] rear face coordinates in mm (centered at [0,0])
P2= vertex(7:end,:)*10/(detector.step);
detector.depth= P2(end,end);
% shift x coordinates to positive values
P1(:,1)= P1(:,1) + vide.left + detector.length /2;
P2(:,1)= P2(:,1) + vide.left + detector.length /2;
% shift y coordinates to positive values
P1(:,2)= P1(:,2)+ vide.up + detector.height /2;
P2(:,2)= P2(:,2)+ vide.up + detector.height /2;
% Number of columns and lines
dimension.nrows= vide.up       + detector.height + vide.down;
dimension.nwcols= vide.left    + detector.length + vide.right;
dimension.nprofs= vide.forward + detector.depth  + vide.back;
%-------------------------------------------------------------------------------
%- anode
%-------------------------------------------------------------------------------
prompt= {'Enter distance to the cathode, in mm:','Enter anode radius, in mm:'};
answer= inputdlg(prompt, msg, 1, {'13' '5'});
if isempty(answer),return,end
dist= eval(answer{1})*10/(detector.step);
prof= eval(answer{2})*10/(detector.step);
%-------------------------------------------------------------------------------
%- segmentation in z
%-------------------------------------------------------------------------------
prompt= {'First layer length, in mm:', 'Second layer length, in mm:', ...
    'Third layer length, in mm:','Fourth layer length, in mm:',...
    'Fifth layer length, in mm:','Sixth layer length, in mm:'};
def= {'8' '13' '15' '18' '18' '18'};
answer= inputdlg(prompt, msg, 1, def);
if isempty(answer),return,end
for i=1:6,
    depth(i)= eval(answer{i})*10/(detector.step);
end
depth= cumsum(depth);
if depth(end)~= detector.depth,
    error('dimensions don''t match geometry'),
end

%-------------------------------------------------------------------------------
%- isolant
%-------------------------------------------------------------------------------
prompt= {'Consider isolant? (y/n):','Thickness','Enter value, in cm-3:'};
answer= inputdlg(prompt, msg, 1, {'n','2', num2str(1e10)});
if isempty(answer),return,end
considerIsolant= strcmp(answer{1},'y');
impurities.iso= eval(answer{3})*(1e-3)*(10/detector.step)^3; % cm3 -> grid3
impurities.isothick= eval(answer{2});


answer= questdlg('Add a "wire" (with same voltage as inner contact)','Electrode',...
    'yes','no','no');
if isempty(answer) return, end
considerElectrode= strcmp(answer,'yes');

datapath= [handles.datapath, handles.detector, filesep];
location= [datapath, 'geometry.mat'];
[filename, pathname]= uiputfile(location, 'Save geometry data ... ');
if filename==0, return, end

%===============================================================================
%- matrices creation
%===============================================================================
tic
%-------------------------------------------------------------------------------
%- initialisation
%-------------------------------------------------------------------------------
y= 1:dimension.nrows;      x= 1:dimension.nwcols;
y_= 2:dimension.nrows-1;   x_= 2:dimension.nwcols-1;    z_= 2:dimension.nprofs-1;
[a,b]= meshgrid(x,y);
% Capsule - Set to ground
capsule= uint8(zeros(dimension.nrows,dimension.nwcols,dimension.nprofs));
% Electrodes to straighten the electric field lines
electrode= single(zeros(dimension.nrows,dimension.nwcols,dimension.nprofs));

%-------------------------------------------------------------------------------
%- cathode
%-------------------------------------------------------------------------------
cylinder= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
t= 0:pi/1000:2*pi;
yv= round(cos(t)*40*10/(detector.step) + vide.up   + (detector.height)/2);
xv= round(sin(t)*40*10/(detector.step) + vide.left + (detector.length) /2);
volume= inpolygon(a,b,xv,yv);
for z= vide.forward+1:vide.forward+detector.depth,
    cylinder(:,:,z)= volume;
end

involume= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
for z=1:detector.depth,
    xv=[z*(P2(1,1)-P1(1,1))/(90*10/(detector.step))+P1(1,1),...
        z*(P2(2,1)-P1(2,1))/(90*10/(detector.step))+P1(2,1),...
        z*(P2(3,1)-P1(3,1))/(90*10/(detector.step))+P1(3,1),...
        z*(P2(4,1)-P1(4,1))/(90*10/(detector.step))+P1(4,1),...
        z*(P2(5,1)-P1(5,1))/(90*10/(detector.step))+P1(5,1),...
        z*(P2(6,1)-P1(6,1))/(90*10/(detector.step))+P1(6,1)];
    yv=[z*(P2(1,2)-P1(1,2))/(90*10/(detector.step))+P1(1,2),...
        z*(P2(2,2)-P1(2,2))/(90*10/(detector.step))+P1(2,2),...
        z*(P2(3,2)-P1(3,2))/(90*10/(detector.step))+P1(3,2),...
        z*(P2(4,2)-P1(4,2))/(90*10/(detector.step))+P1(4,2),...
        z*(P2(5,2)-P1(5,2))/(90*10/(detector.step))+P1(5,2),...
        z*(P2(6,2)-P1(6,2))/(90*10/(detector.step))+P1(6,2)];
    involume(:,:,z+vide.forward)= inpolygon(a,b,xv,yv);
end
involume= involume.*cylinder;
cathode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
cathode1= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
cathode2= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
y_= 2:dimension.nrows-1;   x_= 2:dimension.nwcols-1;    z_= 2:dimension.nprofs-1;
cathode1(y_,x_,z_)= ((involume(y_,x_,z_)==1)) & ...
    ((involume(y_-1,x_,z_)~=involume(y_+1,x_,z_)) | ...
     (involume(y_,x_-1,z_)~=involume(y_,x_+1,z_)) | ...
     (involume(y_-1,x_-1,z_)~=involume(y_+1,x_+1,z_)) | ...
     (involume(y_-1,x_+1,z_)~=involume(y_+1,x_-1,z_))); 
z_=[2:detector.depth-2]; %z_= [2:dimension.nprofs/2];
cathode2(y_,x_,z_)= ((involume(y_,x_,z_)==1)) & ... 
    ((involume(y_,x_+1,z_-1)~=involume(y_,x_-1,z_+1)) | ...
     (involume(y_,x_-1,z_-1)~=involume(y_,x_+1,z_+1)) | ...
     (involume(y_+1,x_,z_-1)~=involume(y_-1,x_,z_+1)) | ...
     (involume(y_-1,x_,z_-1)~=involume(y_+1,x_,z_+1)) | ... 
     (involume(y_-1,x_-1,z_-1)~=involume(y_+1,x_+1,z_+1)) | ...
     (involume(y_+1,x_+1,z_-1)~=involume(y_-1,x_-1,z_+1)) | ...
     (involume(y_+1,x_-1,z_-1)~=involume(y_-1,x_+1,z_+1)) | ...
     (involume(y_-1,x_+1,z_-1)~=involume(y_+1,x_-1,z_+1)) ); 
cathode= cathode1|cathode2;
clear cathode1 cathode2

%-------------------------------------------------------------------------------
%- segmentation in z
%-------------------------------------------------------------------------------
layer= zeros(size(cathode), 'single');
layer(:,:,[vide.forward+1           :vide.forward+depth(1)])= 1;    % segment ID
layer(:,:,[vide.forward+(depth(1)+1):vide.forward+depth(2)])= 7;
layer(:,:,[vide.forward+(depth(2)+1):vide.forward+depth(3)])= 13;
layer(:,:,[vide.forward+(depth(3)+1):vide.forward+depth(4)])= 19;
layer(:,:,[vide.forward+(depth(4)+1):vide.forward+depth(5)])= 25;
layer(:,:,[vide.forward+(depth(5)+1):vide.forward+depth(6)])= 31;
cathode= uint8(layer.*cathode);

center= [vide.up   + (detector.height+1)/2, vide.left + (detector.length+1)/2]; %[y x]
count= 0; %debug
valangle= [0 1 2 3 4 5];
for k=vide.forward+1:vide.forward+depth(6)
    [y x]= find(cathode(:,:,k)>0);
    angles= getAngles(P1, P2, k, center, 0);
    angles= sort(angles);
    for u=1:length(y)
        [angle, r]= cart2pol(y(u)-center(1), x(u)-center(2));
        if angle<0
            angle= 2*pi+angle;
        end
        for camembert=1:5
            if angle>=angles(camembert) & angle<angles(camembert+1)
                cathode(y(u),x(u),k)= cathode(y(u),x(u),k)+valangle(camembert);
                count= count+1;
                break;
            end
        end
        if (angle>=angles(6) || angle<angles(1))
            cathode(y(u),x(u),k)= cathode(y(u),x(u),k)+valangle(6);
            count= count+1; %debug
        end
    end
end
cathodePoints= length(find(cathode>0));
if count~= cathodePoints;
    fprintf('[error] CATHODE missing points [%d expected]: %d found\n', cathodePoints, count);
end

%-------------------------------------------------------------------------------
%- external cathode
%-------------------------------------------------------------------------------
cathode_ext=zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
cathode1= cathode_ext;
cathode2= cathode_ext;
y_= 2:dimension.nrows-1;   x_= 2:dimension.nwcols-1;    z_= 2:dimension.nprofs-1;
cathode1(y_,x_,z_)= ((involume(y_,x_,z_)==0))& ...
    ((involume(y_-1,x_,z_)~=involume(y_+1,x_,z_))|(involume(y_,x_-1,z_)~=involume(y_,x_+1,z_)));
z_=[2:detector.depth-2];
cathode2(y_,x_,z_)=((involume(y_,x_,z_)==0))& ...
    ((involume(y_-1,x_,z_)~=involume(y_+1,x_,z_))|(involume(y_,x_,z_-1)~=involume(y_,x_,z_+1))|...
    (involume(y_,x_-1,z_)~=involume(y_,x_+1,z_)));
cathode_ext = double(cathode1|cathode2);

clear cathode1 cathode2
%-------------------------------------------------------------------------------
%- anode
%-------------------------------------------------------------------------------
tube= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
anode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs, 'single');
anode1= anode;
anode2= anode;
t=[0:pi/1000:2*pi];
for z= vide.forward+1+dist:vide.forward+dist+prof
    rayon= prof*sin(.3*pi*((z-(vide.forward+dist))/prof));
    yv= round(cos(t)*rayon + vide.up   + (detector.height) /2);
    xv= round(sin(t)*rayon + vide.left + (detector.length) /2);
    tube(:,:,z)=inpolygon(a,b,xv,yv);
end
for z= vide.forward+prof+dist+1:vide.forward+detector.depth
    tube(:,:,z)= tube(:,:,vide.forward+prof+dist);
end
involume= involume.*not(tube);

x= round([dimension.nwcols/2-10:dimension.nwcols/2+10]);
y= round([dimension.nrows/2-10 :dimension.nrows/2+10]);
% z= [2:dimension.nprofs-1];
% anode1(y,x,z)= (involume(y,x,z)==1)&...
%     ((involume(y-1,x,z)~= involume(y+1,x,z))|(involume(y,x-1,z)~= involume(y,x+1,z)));
% z= [vide.forward+5:50];
% anode2(y,x,z)= (involume(y,x,z)==1)&...
%     ((involume(y-1,x,z)~= involume(y+1,x,z))|...
%     (involume(y,x,z-1)~= involume(y,x,z+1))|...
%     (involume(y,x-1,z)~= involume(y,x+1,z)));
z= [2:dimension.nprofs-1];
anode1(y,x,z)= ((involume(y,x,z)==1)) & ...
    ((involume(y-1,x,z)~=involume(y+1,x,z)) | ...
     (involume(y,x-1,z)~=involume(y,x+1,z)) | ...
     (involume(y-1,x-1,z)~=involume(y+1,x+1,z)) | ...
     (involume(y-1,x+1,z)~=involume(y+1,x-1,z))); 
z=[vide.forward+5:50]; %z_= [2:dimension.nprofs/2];
anode2(y,x,z)= ((involume(y,x,z)==1)) & ... 
    ((involume(y,x+1,z-1)~=involume(y,x-1,z+1)) | ...
     (involume(y,x-1,z-1)~=involume(y,x+1,z+1)) | ...
     (involume(y+1,x,z-1)~=involume(y-1,x,z+1)) | ...
     (involume(y-1,x,z-1)~=involume(y+1,x,z+1)) | ... 
     (involume(y-1,x-1,z-1)~=involume(y+1,x+1,z+1)) | ...
     (involume(y+1,x+1,z-1)~=involume(y-1,x-1,z+1)) | ...
     (involume(y+1,x-1,z-1)~=involume(y-1,x+1,z+1)) | ...
     (involume(y-1,x+1,z-1)~=involume(y+1,x-1,z+1)) ); 

anode= uint8(anode1|anode2);

%-------------------------------------------------------------------------------
%- external anode
%-------------------------------------------------------------------------------
anode_ext= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
anode1= anode_ext;
anode2= anode_ext;
x= [dimension.nwcols/2-10:dimension.nwcols/2+10];
y= [dimension.nrows/2-10 :dimension.nrows/2+10];
z= [2:dimension.nprofs-1];
anode1(y,x,z)= ((involume(y,x,z)==0))&...
    ((involume(y-1,x,z)~=involume(y+1,x,z))|(involume(y,x-1,z)~=involume(y,x+1,z)));
z=[vide.forward+5:50];
anode2(y,x,z)= ((involume(y,x,z)==0))&...
    ((involume(y-1,x,z)~=involume(y+1,x,z))|(involume(y,x,z-1)~=involume(y,x,z+1))|...
    (involume(y,x-1,z)~=involume(y,x+1,z)));
anode_ext= double(anode1|anode2);

%-------------------------------------------------------------------------------
%- isolant matrix
%-------------------------------------------------------------------------------
isolant= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
if considerIsolant
    for i= 0:impurities.isothick-1
        isolant(:,:,vide.forward+detector.depth-i)= 1;    % ith passivated layer
    end
    %isolant extend over cathode_ext and anode_ext
    isolant= isolant.*(involume + cathode_ext + anode_ext);
    isolant= isolant.*impurities.iso;
end

%-------------------------------------------------------------------------------
%- impurities density matrix
%-------------------------------------------------------------------------------
densge= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
involume= double(involume); % otherwise computation is done in single !
impurities_inz= impurities.low + ...
    (impurities.high-impurities.low)*((1:dimension.nprofs)-vide.forward)/detector.depth;
% impurities_inz= impurities.high + ... % old version
%     (impurities.low-impurities.high)*((1:dimension.nprofs)-vide.forward)/detector.depth;
for z= 1:dimension.nprofs
    densge(:,:,z)= involume(:,:,z)*impurities_inz(z);
    cathode_ext(:,:,z)= cathode_ext(:,:,z)*impurities_inz(z);
    anode_ext(:,:,z) = anode_ext(:,:,z)*impurities_inz(z);
end

%-------------------------------------------------------------------------------
%- package matrix
%-------------------------------------------------------------------------------
package= (zeros(dimension.nrows,dimension.nwcols,dimension.nprofs));
package(1,[1:dimension.nwcols],[1:dimension.nprofs])= 1;
package(dimension.nrows,[1:dimension.nwcols],[1:dimension.nprofs])= 1;
package([1:dimension.nrows],1,[1:dimension.nprofs])= 1;
package([1:dimension.nrows],dimension.nwcols,[1:dimension.nprofs])= 1;
package([1:dimension.nrows],[1:dimension.nwcols],1)= 1;
package([1:dimension.nrows],[1:dimension.nwcols],dimension.nprofs)= 1;
package= uint8(package);

%-------------------------------------------------------------------------------
%- electrode matrix
%-------------------------------------------------------------------------------
if considerElectrode
    electrode(52:53, 52:53, vide.forward + detector.depth:end-1)= voltage.anode;
end
%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
geometryname= handles.detector;
save([pathname,filename],...
    'electrode','detector','vide','voltage','cathode','anode','cathode_ext', 'anode_ext',...
    'capsule','densge',...
    'package','isolant','temperature','dimension','impurities','geometryname')
cd([handles.currentpath])

timeElapsed= toc;
computationduration('Geometry computation', timeElapsed);
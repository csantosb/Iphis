function planar(handles, postfix)
%planar - creation routine for a planar detector
%       o handles    structure with handles and user data (see GUIDATA)
%       o postfix    '2x2' means a planar ith 4 segments

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.1 $Date: december 2005$

% revision 0.1: added external cathode and anode

msg= 'Modify default value(s) ... ?';

%-------------------------------------------------------------------------------
%- if more than one values file, ask which one to choose
%-------------------------------------------------------------------------------
defaultValue= checkDefaultFile(handles, handles.detector);
currentValue= defaultValue;
%-------------------------------------------------------------------------------
%- detector dimensions
%-------------------------------------------------------------------------------
prompt={'Length, in mm:','Height, in mm:','Depth, in mm:','Grid size, in mm:'};
answer= inputdlg(prompt, msg, 1, ...
    {num2str(defaultValue.length),num2str(defaultValue.height),...
    num2str(defaultValue.depth),num2str(defaultValue.gridsize)});
if isempty(answer),return,end

currentValue.length= eval(answer{1});
currentValue.height= eval(answer{2});
currentValue.depth= eval(answer{3});
currentValue.gridsize= eval(answer{4});

detector.step= currentValue.gridsize*10;  % number of steps/0.1mm (1mm == 10 *0.1mm)
scale= 10/detector.step;
detector.length= currentValue.length*scale;
detector.height= currentValue.height*scale;
detector.depth= currentValue.depth*scale;
%-------------------------------------------------------------------------------
%- voltage and impurities
%-------------------------------------------------------------------------------
prompt={'Temperature, in �K:','Anode voltage, in volts:','Cathode voltage, in volts:',...
    '(Near anode) Impurity concentration, in cm-3:', ...
    '(Near cathode) Impurity concentration, in cm-3:'};
answer=inputdlg(prompt, msg, 1, ...
    {num2str(defaultValue.temperature),num2str(defaultValue.anodevoltage),...
    num2str(defaultValue.cathodevoltage),num2str(defaultValue.anodeimpurities), ...
    num2str(defaultValue.cathodeimpurities)});
if isempty(answer),return,end

currentValue.temperature= eval(answer{1});
currentValue.anodevoltage= eval(answer{2});
currentValue.cathodevoltage= eval(answer{3});
currentValue.anodeimpurities= eval(answer{4});
currentValue.cathodeimpurities= eval(answer{5});

% Temperature
temperature= currentValue.temperature;
% High Voltage
voltage.anode= currentValue.anodevoltage;
voltage.cathode= currentValue.cathodevoltage;
% Density of Impurities in Ge (Impurities  cm3 ----> 1/10 mm3)
impurities.low= currentValue.cathodeimpurities*(1e-6)*(detector.step^3);    
impurities.high= currentValue.anodeimpurities*(1e-6)*(detector.step^3);
impurities.material= 'germanium';

%-------------------------------------------------------------------------------
%- space surrounding the detector
%-------------------------------------------------------------------------------
prompt={'Foward space surrounding, in mm:', 'Back space surrounding, in mm:',...
    'Up space surrounding, in mm:', 'Down space surrounding, in mm:',...
    'Right space surrounding, in mm:', 'Left space surrounding, in mm:'};
def={num2str(defaultValue.forward), num2str(defaultValue.back), ...
    num2str(defaultValue.up), num2str(defaultValue.down), ...
    num2str(defaultValue.right), num2str(defaultValue.left)};
answer=inputdlg(prompt, msg,1,def);
if isempty(answer),return,end

currentValue.forward= eval(answer{1});
currentValue.back= eval(answer{2});
currentValue.up= eval(answer{3});
currentValue.down= eval(answer{4});
currentValue.right= eval(answer{5});
currentValue.left= eval(answer{6});

vide.forward= currentValue.forward *scale;
vide.back   = currentValue.back *scale;
vide.up     = currentValue.up *scale;
vide.down   = currentValue.down *scale;
vide.right  = currentValue.right *scale;
vide.left   = currentValue.left *scale;
% Number of columns and lines
dimension.nrows= vide.up + detector.height + vide.down;
dimension.nwcols= vide.left + detector.length + vide.right;
dimension.nprofs= vide.forward + detector.depth + vide.back;

%-------------------------------------------------------------------------------
%- capsule and safeguard rings
%-------------------------------------------------------------------------------
capsule= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);  
% cathode
ButtonName= questdlg('Safeguard Ring at the Cathode?',' ','Yes','No','Exit',... 
    defaultValue.ringcathode);
if strcmp(ButtonName,'Exit'), return, end
currentValue.ringcathode= ButtonName;

if strcmp(currentValue.ringcathode, 'Yes')
    answer= inputdlg({'Size, in mm'}, msg, 1 ,...
        {num2str(defaultValue.ringcathodesize)});
    if isempty(answer),return,end
    
    currentValue.ringcathodesize= eval(answer{1});
    ring_c= currentValue.ringcathodesize*scale;
    capsule([vide.up+detector.height],[vide.left+1:...
        vide.left+ring_c,vide.left+detector.length-ring_c+1:vide.left+detector.length],...
        [vide.forward+1:vide.forward+detector.depth])=1;
    capsule([vide.up+detector.height],[vide.left+1:vide.left+detector.length],... 
        [vide.forward+1:vide.forward+ring_c,vide.forward+detector.depth-ring_c+1:...
        vide.forward+detector.depth])=1;
else % no safeguard ring at cathode
    ring_c=0;
end

% anode
ButtonName=questdlg('Safeguard Ring at the Anode?', ' ', 'Yes','No','Exit', ...
    defaultValue.ringanode);
if strcmp(ButtonName,'Exit'), return, end
currentValue.ringanode= ButtonName;

if strcmp(currentValue.ringanode, 'Yes')
    answer= inputdlg({'Size, in mm'}, msg, 1 ,...
        {num2str(defaultValue.ringanodesize)});
    if isempty(answer),return,end
    
    currentValue.ringanodesize= eval(answer{1});
    ring_a= currentValue.ringanodesize*scale;    
    capsule([vide.up+1],[vide.left+1:vide.left+ring_a,vide.left+detector.length-ring_a+1:...
        vide.left+detector.length],[vide.forward+1:vide.forward+detector.depth])=1;
    capsule([vide.up+1],[vide.left+1:vide.left+detector.length],...
        [vide.forward+1:vide.forward+ring_a,vide.forward+detector.depth-ring_a+1:...
        vide.forward+detector.depth])=1;
else % no safeguard ring at anode
    ring_a=0;
end
capsule=uint8(capsule);

%-------------------------------------------------------------------------------
%- electrodes to straighten the electric field lines
%-------------------------------------------------------------------------------
ButtonName=questdlg('Use electrodes?','  ','Yes','No','Exit',defaultValue.useelectrode);
electrode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
currentValue.useelectrode= ButtonName;
if strcmp(ButtonName,'Exit'),return, end

if strcmp(currentValue.useelectrode, 'Yes')
    fprintf('[ATTENTION]This code has not be correctly checked for step != 1mm\n');
    prompt={'kk','First electrode, in mm:','Second electrode, in mm:','Third electrode, in mm:',...
        'Fourth electrode, in mm:','Fifth electrode, in mm:','Sixth electrode, in mm:'};
    answer=inputdlg(prompt, msg,1, ...
        {int2str(defaultValue.kk), int2str(defaultValue.e1), int2str(defaultValue.e2),...
        int2str(defaultValue.e3), int2str(defaultValue.e4),int2str(defaultValue.e5),...
        int2str(defaultValue.e6)});
    if isempty(answer),return,end

    currentValue.kk= eval(answer{1});
    currentValue.e1= eval(answer{2});
    currentValue.e2= eval(answer{3});
    currentValue.e3= eval(answer{4});
    currentValue.e4= eval(answer{5});
    currentValue.e5= eval(answer{6});
    currentValue.e6= eval(answer{7});
        
    kk= currentValue.kk*scale; 
    d1= currentValue.e1*scale; 
    d2= currentValue.e2*scale; 
    d3= currentValue.e3*scale;
    d4= currentValue.e4*scale; 
    d5= currentValue.e5*scale; 
    d6= currentValue.e6*scale;

    electrode(vide.up+d1,[vide.left-1:vide.left+detector.length+2],vide.forward-1)= ...
        voltage.cathode*0.1*kk;
    electrode(vide.up+d1,[vide.left-1:vide.left+detector.length+2],...
        vide.forward+detector.depth+2)=voltage.cathode*0.1*kk;
    electrode(vide.up+d1,vide.left-1,vide.forward-1:vide.forward+detector.depth+2)= ...
        voltage.cathode*0.1*kk;
    electrode(vide.up+d1,vide.left+detector.length+2,...
        vide.forward-1:vide.forward+detector.depth+2)=voltage.cathode*0.1*kk;

    electrode(vide.up+d2,[vide.left-1:vide.left+detector.length+2],vide.forward-1)=...
        voltage.cathode*0.33*kk;
    electrode(vide.up+d2,[vide.left-1:vide.left+detector.length+2],...
        vide.forward+detector.depth+2)=voltage.cathode*0.33*kk;
    electrode(vide.up+d2,vide.left-1,vide.forward-1:vide.forward+detector.depth+2)=...
        voltage.cathode*0.33*kk;
    electrode(vide.up+d2,vide.left+detector.length+2,...
        vide.forward-1:vide.forward+detector.depth+2)=voltage.cathode*0.33*kk;

    electrode(vide.up+d3,[vide.left-1:vide.left+detector.length+2],vide.forward-1)=...
        voltage.cathode*0.6*kk;
    electrode(vide.up+d3,[vide.left-1:vide.left+detector.length+2],...
        vide.forward+detector.depth+2)=voltage.cathode*0.6*kk;
    electrode(vide.up+d3,vide.left-1,vide.forward-1:vide.forward+detector.depth+2)=...
        voltage.cathode*0.6*kk;
    electrode(vide.up+d3,vide.left+detector.length+2,...
        vide.forward-1:vide.forward+detector.depth+2)=voltage.cathode*0.6*kk;

    electrode(vide.up+d4,[vide.left-1:vide.left+detector.length+2],vide.forward-1)=...
        voltage.cathode*0.8*kk;
    electrode(vide.up+d4,[vide.left-1:vide.left+detector.length+2],...
        vide.forward+detector.depth+2)=voltage.cathode*0.8*kk;
    electrode(vide.up+d4,vide.left-1,vide.forward-1:vide.forward+detector.depth+2)=...
        voltage.cathode*0.8*kk;
    electrode(vide.up+d4,vide.left+detector.length+2,...
        vide.forward-1:vide.forward+detector.depth+2)=voltage.cathode*0.8*kk;

    electrode(vide.up+d5,[vide.left-1:vide.left+detector.length+2],vide.forward-1)=...
        voltage.cathode*kk;
    electrode(vide.up+d5,[vide.left-1:vide.left+detector.length+2],...
        vide.forward+detector.depth+2)=voltage.cathode*kk;
    electrode(vide.up+d5,vide.left-1,vide.forward-1:vide.forward+detector.depth+2)=...
        voltage.cathode*kk;
    electrode(vide.up+d5,vide.left+detector.length+2,...
        vide.forward-1:vide.forward+detector.depth+2)=voltage.cathode*kk;
end

%-------------------------------------------------------------------------------
%- cathode
%-------------------------------------------------------------------------------
answer=inputdlg({'Number of segments on a row (cathode)?'}, msg, 1, ...
    {num2str(defaultValue.nbsegmentcathode)});
if isempty(answer),return,end

currentValue.nbsegmentcathode= eval(answer{1});
segment= currentValue.nbsegmentcathode;

cathode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp =  zeros(dimension.nrows,dimension.nwcols,dimension.nprofs,segment);
temp2=  zeros(dimension.nrows,dimension.nwcols,dimension.nprofs,segment);

distance1=(detector.length-ring_c*2)/segment;
distance2=(detector.depth -ring_c*2)/segment;
if round(distance1)~=distance1 | round(distance2)~=distance2
    return
end

for i=1:segment
    temp ([vide.up+detector.height],...
        [vide.left+ring_c+1+distance1*(i-1):vide.left+ring_c+distance1*i],...
        [vide.forward+1:vide.forward+detector.depth],i)=1;
    temp2([vide.up+detector.height],[vide.left+1:vide.left+detector.length],...
        [vide.forward+ring_c+1+distance2*(i-1):vide.forward+ring_c+distance2*i],i)=1;
end
count=0;
for i=1:segment
    for j=1:segment
        count=count+1;
        cathode=cathode+temp(:,:,:,i).*temp2(:,:,:,j)*count;
    end
end
cathode= uint8(cathode.*not(capsule==1));

%-------------------------------------------------------------------------------
%- anode
%-------------------------------------------------------------------------------
answer=inputdlg({'Number of segments on a row (anode)?'}, msg, 1, ...
    {num2str(defaultValue.nbsegmentanode)});
if isempty(answer),return,end

currentValue.nbsegmentanode= eval(answer{1});
segment= currentValue.nbsegmentanode;

anode=zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs,segment);
temp2= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs,segment);

distance1= (detector.length-ring_a*2)/segment;
distance2= (detector.depth -ring_a*2)/segment;

if round(distance1)~=distance1 | round(distance2)~=distance2
    return
end
for i=1:segment
    temp([vide.up+1],[vide.left+ring_a+1+distance1*(i-1):vide.left+ring_a+distance1*i],...
        [vide.forward+1:vide.forward+detector.depth],i)=1;
    temp2([vide.up+1],[vide.left+1:vide.left+detector.length],...
        [vide.forward+ring_a+1+distance2*(i-1):vide.forward+ring_a+distance2*i],i)=1;
end
count= 0;
for i= 1:segment
    for j= 1:segment
        count= count+1;
        anode= anode+temp(:,:,:,i).*temp2(:,:,:,j)*count;
    end
end
anode=uint8(anode.*not(capsule==1));

%-------------------------------------------------------------------------------
%- external cathode and anode
%-------------------------------------------------------------------------------
% cathode external
cathode_ext= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
cathode_ext([vide.up+detector.height+1],...
    [vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])= impurities.low;
% anode external
anode_ext= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
anode_ext([vide.up],...
    [vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])= impurities.high;

%-------------------------------------------------------------------------------
%- density of impurities
%-------------------------------------------------------------------------------
densge= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp=zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp([vide.up+1:vide.up+detector.height],[vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])=1;
impurities_inz= impurities.low + ...
    (impurities.high-impurities.low)*...
    ((1:dimension.nrows)- (vide.up+1))/(vide.up+detector.height-(vide.up+1));
for height= 1:dimension.nrows
    densge(height,:,:)= impurities_inz(height);
end
densge= densge.*temp;

%-------------------------------------------------------------------------------
%- package
%-------------------------------------------------------------------------------
package= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
package(1,[1:dimension.nwcols],[1:dimension.nprofs])=1;
package(dimension.nrows,[1:dimension.nwcols],[1:dimension.nprofs])=1;
package([1:dimension.nrows],1,[1:dimension.nprofs])=1;
package([1:dimension.nrows],dimension.nwcols,[1:dimension.nprofs])=1;
package([1:dimension.nrows],[1:dimension.nwcols],1)=1;
package([1:dimension.nrows],[1:dimension.nwcols],dimension.nprofs)=1;
package= uint8(package);

%-------------------------------------------------------------------------------
%- isolant
%-------------------------------------------------------------------------------
prompt= {'Consider isolant ? (y / n):', 'Value, in cm-3:'};
answer= inputdlg(prompt, msg, 1, {defaultValue.considerisolant, ...
    num2str(defaultValue.isolant)});

if isempty(answer),return,end
currentValue.considerisolant= answer{1};
currentValue.isolant= eval(answer{2});

if strcmp(currentValue.considerisolant, 'y')
    impurities.iso= currentValue.isolant*(1e-3)*(10/detector.step)^3; % impurities cm3 -> grid^3
    isolant= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
    isolant(vide.up+2:vide.up+detector.height-1,vide.left+1,...
        vide.forward+1:vide.forward+detector.depth)= impurities.iso;
    isolant(vide.up+2:vide.up+detector.height-1,vide.left+detector.length,...
        vide.forward+1:vide.forward+detector.depth)= impurities.iso;
    isolant(vide.up+2:vide.up+detector.height-1,vide.left+1:vide.left+detector.length,...
        vide.forward+1)= impurities.iso;
    isolant(vide.up+2:vide.up+detector.height-1,vide.left+1:vide.left+detector.length,...
        vide.forward+detector.depth)= impurities.iso;
end

%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
geometryname= handles.detector;
cd([handles.datapath, filesep, handles.detector]);
[filename, pathname]= uiputfile('*.mat', 'Save geometry data', 'geometry.mat');
save([pathname,filename],...
    'electrode','detector','vide','voltage','cathode','anode','capsule','densge',...
    'anode_ext', 'cathode_ext', 'package','isolant',...
    'temperature', 'dimension','impurities','geometryname')
cd(handles.currentpath);

%-------------------------------------------------------------------------------
%- check currentValue
%-------------------------------------------------------------------------------
check= (defaultValue.length~= currentValue.length);
check= check + (defaultValue.height~= currentValue.height);
check= check + (defaultValue.depth~= currentValue.depth);
check= check + (defaultValue.gridsize~= currentValue.gridsize);

check= check + (defaultValue.temperature~= currentValue.temperature);        
check= check + (defaultValue.anodevoltage~= currentValue.anodevoltage);          
check= check + (defaultValue.cathodevoltage~= currentValue.cathodevoltage);           
check= check + (defaultValue.anodeimpurities~= currentValue.anodeimpurities);       
check= check + (defaultValue.cathodeimpurities~= currentValue.cathodeimpurities);          

check= check + (defaultValue.forward~= currentValue.forward);
check= check + (defaultValue.back~= currentValue.back);
check= check + (defaultValue.up~= currentValue.up);
check= check + (defaultValue.down~= currentValue.down);
check= check + (defaultValue.right~= currentValue.right);
check= check + (defaultValue.left~= currentValue.left);

check= check + ~strcmp(defaultValue.ringcathode, defaultValue.ringcathode);
check= check + (defaultValue.ringcathodesize~= currentValue.ringcathodesize);
check= check + ~strcmp(defaultValue.ringanode, defaultValue.ringanode);
check= check + (defaultValue.ringanodesize~= currentValue.ringanodesize);

check= check + ~strcmp(defaultValue.useelectrode, currentValue.useelectrode);
check= check + (defaultValue.kk~= currentValue.kk);
check= check + (defaultValue.e1~= currentValue.e1);
check= check + (defaultValue.e2~= currentValue.e2);
check= check + (defaultValue.e3~= currentValue.e3);
check= check + (defaultValue.e4~= currentValue.e4);
check= check + (defaultValue.e5~= currentValue.e5);
check= check + (defaultValue.e6~= currentValue.e6);

check= check + (defaultValue.nbsegmentcathode~= currentValue.nbsegmentcathode);
check= check + (defaultValue.nbsegmentanode~= currentValue.nbsegmentanode);

check= check + ~strcmp(defaultValue.considerisolant, currentValue.considerisolant);
check= check + (defaultValue.isolant~= currentValue.isolant);

if check % currentValue is different from defaultValue
    cd(handles.parameterpath);
    [filename, pathname]= uiputfile('*.mat', 'Save your parameters', ...
        ['*', handles.detector,'.mat']);
    save([pathname,filename], 'defaultValue');
    cd(handles.currentpath);
end

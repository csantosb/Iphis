function agata__red(handles)
%agata__red - call the creation routine for agata red (assymetric)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

agata__angle(handles,'red');
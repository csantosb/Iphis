function planar2x2(handles)
%planar2x2 - call the creation routine for a planar with 4 segments
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

planar(handles, '2x2');
function agata__yellow(handles)
%agata__yellow - call the creation routine for agata yellow (symetric)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

agata__angle(handles, 'yellow');
function defaultValue= checkDefaultFile(handles, detector)
%checkDefaultFile - check if a default file exist for this detector
%   For each detector, at least there is a default value file. Each time a
%   user modifies these values during the creation of the detector, he has
%   the opportunity to save his values.
%   Original default file: default<detector>.mat
%   User default file: <name><detector>.mat
%       o handles    structure with handles and user data (see GUIDATA)
%       o detector   the detector to check
%   Outputs
%       o defaultValue strcuture containing the default values to use

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

%-------------------------------------------------------------------------------
%- find value files
%-------------------------------------------------------------------------------
filelist= dir([handles.parameterpath, '*', detector, '*.mat']);
defaultfile= ['default', detector, '2x2.mat'];
defaultfilerank= findDefaultfilerank(filelist, defaultfile);

if isempty(filelist) | defaultfilerank==0
    status_Callback(handles, 'Default values not found', ' ', ' ', 1);
    return
elseif size(filelist,1)==1 & defaultfilerank==1
    load([handles.parameterpath, defaultfile]);
else
    %-------------------------------------------------------------------------------
    %- let the user choose the file to load
    %-------------------------------------------------------------------------------
    [filerank, answer]= listdlg('PromptString','Select a file:',...
        'SelectionMode','single', 'InitialValue', defaultfilerank, ...
        'ListString', {filelist.name});
    if answer==1
       load([handles.parameterpath, filelist(filerank).name]);
    else
        return
    end
end

%===============================================================================
function rank= findDefaultfilerank(filelist, defaultfile)
%findDefaultfilerank
%   find where the default file is in the list of file
rank= 1;
for i=1:size(filelist,1)
    if strcmp(filelist(i).name, defaultfile)
        return,
    else
        rank= rank+1;
    end
end
rank= 0; % we should not reach this line if the file is found


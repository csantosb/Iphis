function agata__blue(handles)
%agata__blue - call the creation routine for agata blue (assymetric)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$

agata__angle(handles, 'blue');
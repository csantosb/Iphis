function angles= getAngles(P1, P2, z, center, verbose)
%getAngles - computes the coordinates of the radial segmentation at a z level
% from the top and bottom vertex coordinates for Agata detectors
%       o P1    contains close ended vertex coordinates
%       o P2    contains open ended vertex coordinates
%       o z     z plane where you want to compute the segmentation
%       o verbose if '1', plot the results
% Output
%       o angles segmentation angles at z plane
%
% see agata__angle
%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: december 2005$


%-------------------------------------------------------------------------------
%- inverse x and y (for compatibility purposes)
%-------------------------------------------------------------------------------
tmp= P1(:,1);
P1(:,1)= P1(:,2);
P1(:,2)= tmp;

tmp= P2(:,1);
P2(:,1)= P2(:,2);
P2(:,2)= tmp;

%-------------------------------------------------------------------------------
%- build the 6 functions going through the vertex
%-------------------------------------------------------------------------------   
lines= zeros(4,6);
for i=1:6
    x0= P1(i,1); x1= P2(i,1);
    y0= P1(i,2); y1= P2(i,2);
    z0= P1(i,3); z1= P2(i,3);
    ax= (x1-x0)/(z1-z0);
    bx= (z1*x0-z0*x1)/(z1-z0);
    ay= (y1-y0)/(z1-z0);
    by= (z1*y0-z0*y1)/(z1-z0);
    lines(:,i)= [ax bx ay by]';
end

midpoints1= zeros(6,2);
midpoints2= zeros(6,2);
midlines= zeros(4,6);

%-------------------------------------------------------------------------------
%- find the middle of the lines (this is where the segmentation takes
%- place)
%------------------------------------------------------------------------------- 
for i=1:6
    if i==6
        m0=6; m1=1;
    else
        m0=i; m1=i+1;
    end

    x0= P1(m0,1); x1= P1(m1,1);
    y0= P1(m0,2); y1= P1(m1,2);
    xm= x0 + (x1-x0)/2;
    ym= y0 + (y1-y0)/2;
    midpoints1(m0,:)= [xm ym];
    
    x0= P2(m0,1); x1= P2(m1,1);
    y0= P2(m0,2); y1= P2(m1,2);
    xm= x0 + (x1-x0)/2;
    ym= y0 + (y1-y0)/2;
    midpoints2(m0,:)= [xm ym];   
    
    x0= midpoints1(m0,1); x1= midpoints2(m0,1);
    y0= midpoints1(m0,2); y1= midpoints2(m0,2);
    z0= P1(m0,3); z1= P2(m0,3);
    
    ax= (x1-x0)/(z1-z0);
    bx= (z1*x0-z0*x1)/(z1-z0);
    ay= (y1-y0)/(z1-z0);
    by= (z1*y0-z0*y1)/(z1-z0);
    midlines(:,i)= [ax bx ay by]';
end

%-------------------------------------------------------------------------------
%- computes angles values of the segmentations at z
%------------------------------------------------------------------------------- 
angles= [];
points= zeros(6,2);
for i=1:6
    ax= midlines(1,i); bx= midlines(2,i); ay= midlines(3,i); by= midlines(4,i); 
    x= ax*z+bx; y= ay*z+by;
    points(i,:)= [x y];
    [angles(i), r]= cart2pol(x-center(1), y-center(2));
    % tranform angle from [-pi pi] to [0 2pi]
    if angles(i)<0
        angles(i)= 2*pi+angles(i);
    end
end

%-------------------------------------------------------------------------------
%- plot result
%------------------------------------------------------------------------------- 
if verbose
    %%% plot 6 lines
    figure,
    hold on, plot3(P1(:,1), P1(:,2), P1(:,3));
    hold on, plot3(P2(:,1), P2(:,2), P2(:,3));
    hold on, plot3(points(:,1), points(:,2), z*ones(size(points(:,1))),'m');
    hold on, plot3(points(:,1), points(:,2), z*ones(size(points(:,1))),'m.');
    z0= 0; z1= 90;
    for i=1:6
        ax= lines(1,i);
        bx= lines(2,i);
        ay= lines(3,i);
        by= lines(4,i);
        x= [ax*z0+bx ax*z1+bx];
        y= [ay*z0+by ay*z1+by];
        z= [0 90];
        hold on, plot3(x,y,z);
        hold on, plot3(x,y,z, '+');
    end

    for i=1:6
        ax= midlines(1,i);
        bx= midlines(2,i);
        ay= midlines(3,i);
        by= midlines(4,i);
        x= [ax*z0+bx ax*z1+bx];
        y= [ay*z0+by ay*z1+by];
        z= [0 90];
        hold on, plot3(x,y,z, 'r');
        hold on, plot3(x,y,z, 'r+');
    end
  
    disp('Angles'), angles
end

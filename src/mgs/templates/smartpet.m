function smartpet(handles)
%agata__angle - creation routine for agata detectors
%       o handles    structure with handles and user data (see GUIDATA)
%  Specifications are from the University of Liverpool

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: march 2006$

msg= 'Modify default value(s) ... ?';

%-------------------------------------------------------------------------------
%- detector dimensions
%-------------------------------------------------------------------------------
prompt={'Length, in mm:','Height, in mm:','Depth, in mm:','Grid size, in mm:'};
answer= inputdlg(prompt, msg, 1, {'74', '20', '74', '1'});
if isempty(answer),return,end

detector.step= eval(answer{4})*10;  % number of steps/0.1mm (1mm == 10 *0.1mm)
scale= 10/detector.step;
detector.length= eval(answer{1})*scale;
detector.height= eval(answer{2})*scale;
detector.depth= eval(answer{3})*scale;

%-------------------------------------------------------------------------------
%- voltage and impurities
%-------------------------------------------------------------------------------
prompt={'Temperature, in �K:','Anode voltage, in volts:','Cathode voltage, in volts:',...
    '(Near Anode) impurity concentration, in cm-3:', ...
    '(Near Cathode) impurity concentration, in cm-3:'};
answer=inputdlg(prompt, msg, 1, {'78', '0', '-1800', num2str(-1e9), num2str(-5e9)});
if isempty(answer),return,end

temperature= eval(answer{1});
voltage.anode= eval(answer{2});
voltage.cathode= eval(answer{3});
impurities.low= eval(answer{4})*(1e-6)*(detector.step^3);    % Impurities  cm3 ----> 1/10 mm3
impurities.high= eval(answer{5})*(1e-6)*(detector.step^3);
impurities.material= 'germanium';

%-------------------------------------------------------------------------------
%- space surrounding the detector
%-------------------------------------------------------------------------------
prompt={'Enter (forward) space surrounding, in mm:','Enter (back) space surrounding, in mm:',...
    'Enter (up) space surrounding, in mm:','Enter (down) space surrounding, in mm:',...
    'Enter (left) space surrounding, in mm:','Enter (right) space surrounding, in mm:'};
def={num2str(64),num2str(64),num2str(15),num2str(15),num2str(89),num2str(64)};
answer=inputdlg(prompt,msg,1,def);
if isempty(answer),return,end
vide.forward= eval(answer{1})*scale;
vide.back   = eval(answer{2})*scale;
vide.up     = eval(answer{3})*scale;
vide.down   = eval(answer{4})*scale;
vide.left   = eval(answer{5})*scale;
vide.right  = eval(answer{6})*scale;

%%%%%%% Number of columns and lines
dimension.nrows = vide.up      + detector.height + vide.down;
dimension.nwcols= vide.left    + detector.length + vide.right;
dimension.nprofs= vide.forward + detector.depth  + vide.back;

%-------------------------------------------------------------------------------
%- capsule 
%-------------------------------------------------------------------------------
capsule=zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
capsule([vide.up+detector.height],[vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])=1;
capsule([vide.up+1],[vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])=1;
%     capsule([vide.up+detector.height],[vide.left+1:vide.left+5,vide.left+detector.length-4:vide.left+detector.length],...
%         [vide.forward+1:vide.forward+detector.depth])=1;
%     capsule([vide.up+detector.height],[vide.left+1:vide.left+detector.length],...
%         [vide.forward+1:vide.forward+5,vide.forward+detector.depth-4:vide.forward+detector.depth])=1;

%-------------------------------------------------------------------------------
%- no electrodes 
%-------------------------------------------------------------------------------
electrode=uint8(zeros(dimension.nrows,dimension.nwcols,dimension.nprofs));

%-------------------------------------------------------------------------------
%- cathode 
%-------------------------------------------------------------------------------
ring= 7;
strip= 5;
cathode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
counter= 0;
range= [1+ring*scale:strip*scale:detector.depth-ring*scale];
cstlength= [vide.left+1+ring*scale:vide.left+detector.length-ring*scale];
for i= range
    counter= counter+1;
    cathode([vide.up+detector.height],cstlength,...
        [vide.forward+i:vide.forward+i+strip*scale-1])= counter;
end
capsule= capsule.*not(cathode);
cathode= uint8(cathode);

%-------------------------------------------------------------------------------
%- anode 
%-------------------------------------------------------------------------------
anode= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
counter= 0;
range= [1+ring*scale:strip*scale:detector.length-ring*scale];
cstlength= [vide.forward+1+ring*scale:vide.forward+detector.depth-ring*scale];
for i= range
    counter= counter+1;
    anode([vide.up+1],[vide.left+i:vide.left+i+strip*scale-1],...
        cstlength)= counter;
end
capsule=capsule.*not(anode);
anode=uint8(anode);

capsule=uint8(capsule);

%-------------------------------------------------------------------------------
%- external cathode and anode
%-------------------------------------------------------------------------------
% cathode external
cathode_ext= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
cathode_ext([vide.up+detector.height+1],...
    [vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])= impurities.high;
% anode external
anode_ext= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
anode_ext([vide.up],...
    [vide.left+1:vide.left+detector.length],...
    [vide.forward+1:vide.forward+detector.depth])= impurities.low;

%-------------------------------------------------------------------------------
%- density of impurities
%-------------------------------------------------------------------------------
densge= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
temp([vide.up+1:vide.up+detector.height],[vide.left+1:vide.left+detector.length], ...
    [vide.forward+1:vide.forward+detector.depth])=1;
impurities_inz= impurities.low + (impurities.high-impurities.low)*...
    ((1:detector.height) -1)/(detector.height-1);
for i= vide.up+1:vide.up+detector.height
    densge(i,:,:)= impurities_inz(i-vide.up);
end
densge=densge.*temp;

%-------------------------------------------------------------------------------
%- package
%-------------------------------------------------------------------------------
package= zeros(dimension.nrows,dimension.nwcols,dimension.nprofs);
package(1,[1:dimension.nwcols],[1:dimension.nprofs])= 1;
package(dimension.nrows,[1:dimension.nwcols],[1:dimension.nprofs])= 1;
package([1:dimension.nrows],1,[1:dimension.nprofs])= 1;
package([1:dimension.nrows],dimension.nwcols,[1:dimension.nprofs])= 1;
package([1:dimension.nrows],[1:dimension.nwcols],1)= 1;
package([1:dimension.nrows],[1:dimension.nwcols],dimension.nprofs)= 1;
package= uint8(package);


%-------------------------------------------------------------------------------
%- isolant
%-------------------------------------------------------------------------------
impurities.iso= 0;
isolant= uint8(zeros(dimension.nrows,dimension.nwcols,dimension.nprofs));

%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
geometryname= handles.detector;
cd([handles.datapath, filesep, handles.detector]);
[filename, pathname]= uiputfile('*.mat', 'Save geometry data', 'geometry.mat');
save([pathname,filename],...
    'electrode','detector','vide','voltage','cathode','anode','capsule','densge',...
    'anode_ext', 'cathode_ext', 'package','isolant',...
    'temperature', 'dimension', 'impurities', 'geometryname')
cd(handles.currentpath);


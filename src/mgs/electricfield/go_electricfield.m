function go_electricfield(handles)
%go_electricfield - compute the electric field (gradient of the potential
% mapping)
%       o handles    structure with handles and user data (see GUIDATA)

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

%-------------------------------------------------------------------------------
%- load and save data
%-------------------------------------------------------------------------------
fprintf('Computing electric field ...\n');
datapath= [handles.datapath, handles.detector, filesep];

location= [datapath,'geometry.mat'];
[filename, pathname]= uigetfile(location, 'Select your geometry file ... ');
if filename==0,return,
else,load([pathname,filename],'anode_ext','cathode_ext','densge','voltage','detector'),
end
location=[datapath,'potential_mapping.mat'];
[filename, pathname]= uigetfile(location, 'Select your potential mapping file ... ');
if filename==0,return,else,load([pathname,filename],'potential_ext'),end

% where to save the results
location= [datapath, 'efield.mat'];
[filename, pathname]= uiputfile(location, 'Save electric field data ... ');
if filename==0, return, end

%-------------------------------------------------------------------------------
%- computation
%-------------------------------------------------------------------------------
tic
scale= detector.step/100;
% with potential_ext
annotzero= (anode_ext~=0); anzero= not(annotzero);
catnotzero= (cathode_ext~=0); catzero= not(catnotzero);
denotzero= (densge~=0) + (anode_ext~=0) + (cathode_ext~=0); 
dezero= not(denotzero);
%electric field everywhere (EField  V./arbitrary units. -->  V./cm. or V./mm.)
[efield_x,efield_y,efield_z]= gradient(-potential_ext,scale);

% % with potential 
% annotzero= (anode~=0); anzero= not(annotzero);
% catnotzero= (cathode~=0); catzero= not(catnotzero);
% denotzero= (densge~=0); dezero=not(denotzero);
% % electric field eveywhere (EField  V./arbitrary units. -->  V./cm. or V./mm.)
% [efield_x,efield_y,efield_z]= gradient(-potential,scale);

% % electric field all around the Ge volume for the anode
% p_electrode= (potential.*denotzero) + voltage.anode*dezero;
% [efield_xe,efield_ye,efield_ze]= gradient(-p_electrode, scale);
% efield_x= 2*efield_xe.*(annotzero) + efield_x.*anzero;
% efield_y= 2*efield_ye.*(annotzero) + efield_y.*anzero;
% efield_z= 2*efield_ze.*(annotzero) + efield_z.*anzero;
% % electric field all around the Ge volume for the cathode
% p_electrode= (potential.*denotzero) + voltage.cathode*dezero;
% [efield_xe,efield_ye,efield_ze]=gradient(-p_electrode,scale);
% efield_x= 2*efield_xe.*(catnotzero) + efield_x.*catzero;
% efield_y= 2*efield_ye.*(catnotzero) + efield_y.*catzero;
% efield_z= 2*efield_ze.*(catnotzero) + efield_z.*catzero;

% homemade correction
border= annotzero | catnotzero;
inside= (densge~=0);
[maxx maxy maxz]= size(border);
limits= [1.01, sqrt(2)+0.01, 2.01];
plane= [-1 0 1];

for z= 2:maxz-1
    for x= 1:maxx
        for y= 1:maxy
            if border(x,y,z)==1 % (x,y,z) is on the anode or the cathode
                % keepvalue contains (pa,pb,Z, dist) where:
                % - pa, pb are strictly inside the germanium and the
                % closest to (x,y,z)
                % - Z equals z-1 or z or z+1
                % - dist is the computed distance
                keepvalue= [];
                for i=1:3 % used with plane
                    [pa pb]= find(inside(:,:,z+plane(i))); % take all the (x,y,z-1) , (x,y,z), (x,y,z+1) where (x,y,Z) is strictly inside germanium (densge~=0) 
                    if ~isempty(pa)
                        % compute distance from (x,y,Z) to any point inside
                        % Germanium where:
                        % - Z is -1, 0, +1 (so we are looking at maximum z
                        % distance of 1
                        % - (x,y,z) is on the border
                        % - pa, pb is a list of point inside the germanium
                        % and not on the border
                        l= ones(length(pa),1);
                        dist= sqrt((x*l-pa).^2+ (y*l-pb).^2 +(plane(i)*l).^2);
                        % find at least two distances corresponding to a
                        % vertical or an horizontal (<1.01), to a diagonal
                        % (<1.4242), or else (<2.01)
                        keeprank= find(dist<limits(1));
                        if isempty(keeprank) | length(keeprank)==1
                            keeprank= find(dist<limits(2));
                            if isempty(keeprank) | length(keeprank)==1
                                keeprank= find(dist<limits(3));
                            end
                        end
                        l= ones(length(keeprank),1);
                        % add all pa, pb, Z, dist with the minimum distance
                        % found
                        keepvalue= [keepvalue; pa(keeprank) pb(keeprank) (z+plane(i))*l dist(keeprank)];
                    end
                end %i
                if ~isempty(keepvalue)
                    nbValue= size(keepvalue,1);
                    % fieldT is the fieldT value of all points within
                    % distance of (x,y,z) multiplied by the distance (the
                    % farthest the lowest)
                    fieldx= 0;
                    fieldy= 0;
                    fieldz= 0;
                    % baryfactor is the value of all the dividers
                    baryfactor= 0;
                    for i= 1:nbValue
                        fieldx= fieldx + efield_x(keepvalue(i,1), keepvalue(i,2), keepvalue(i,3))/ keepvalue(i,4);
                        fieldy= fieldy + efield_y(keepvalue(i,1), keepvalue(i,2), keepvalue(i,3))/ keepvalue(i,4);
                        fieldz= fieldz + efield_z(keepvalue(i,1), keepvalue(i,2) ,keepvalue(i,3))/ keepvalue(i,4);
                        baryfactor= baryfactor + 1/keepvalue(i,4);
                    end
                    % update fieldT at x y z
                    efield_x(x,y,z)= fieldx/baryfactor;
                    efield_y(x,y,z)= fieldy/baryfactor;
                    efield_z(x,y,z)= fieldz/baryfactor;
                end 
            end % border
        end %y
    end %x
end %z
% end

efield= sqrt((efield_x.*efield_x) + (efield_y.*efield_y) + (efield_z.*efield_z));

if max(efield)>1000,warning('electric field higher than 1000 V/cm'),end

%-------------------------------------------------------------------------------
%- save data
%-------------------------------------------------------------------------------
save([pathname,filename],'efield_x','efield_y','efield_z');  % efield is not saved
timeElapsed= toc; 
computationduration('Electric field', timeElapsed);

update_Callback(handles);



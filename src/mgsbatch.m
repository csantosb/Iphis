function mgsbatch(varargin)
%mgsbatch -  compute trajectories, charges and current from a property
%file. varargin should contain:
%       o properties  property file place in teh same directory
%       o data file   interaction points to compute
% Results:
%       o data saved in ASCII/binary files 
%
% If you suspend the calculus but not change the properties (path and data
% to process), this program will resume and continue the computation.

%  Copyright (C) 2005 mgsteam <mgsteam@ires.in2p3.fr>
%  $Revision: 1.0 $Date: february 2006$

if nargin<2
    fprintf('[mgsbatch] type: mgs_batch mgs.prop xyz.txt ...where:\n');
    fprintf('- mgs.prop contains properties\n');
    fprintf('- xyz.txt contains points to compute\n');
    return;
end

properties= varargin{1}; 
file_to_process= varargin{2};

handles.currentpath= [cd, filesep];
handles.datapath= [handles.currentpath, 'data', filesep];
handles.parameterpath= [handles.currentpath, 'parameters', filesep];
handles.version= 'MGS v5alpha1 IReS/IN2P3';
handles.releasedate= ' February 06';
handles.authors= {'C. Santos', 'P. Medina', 'C. Parisel'};
handles.email= 'mgsteam@ires.in2p3.fr';
handles.detector= '-';              % name of the selected detector
handles.scancoord= 'cartesian';     % 1 if cartesian coordinates, 0 if cylindrical
handles.precision= 'double';        % default
handles.detectortype= 'planar';     % or 'coaxial'
handles.steptime= 1e-9;             % step time in ns (should not be changed)
handles.plotenable= 0;              % plot computation results (0= no)
handles.packsize= 30;               % compute 30 pulses at a time
handles.scanmode= 'charge';         % only charge is computed
handles.scandata= 'ascii';          % 'ascii' or 'binary'

fid= fopen(properties, 'rt');
if (fid==-1) fprintf('[Error] Can''t open property file: %s\n', properties); return; end
%prop= textscan(fid,'%s %s','commentStyle', '//');  % matlab 7
[prop{1:2}]= textread(properties, '%s %s', 'commentstyle', 'c++');
handles.scanmode= char(prop{2}(find(strcmp(prop{1},'method='))));
handles.precision= char(prop{2}(find(strcmp(prop{1},'precision='))));
handles.scandata= char(prop{2}(find(strcmp(prop{1},'data_format='))));
handles.detector= char(prop{2}(find(strcmp(prop{1},'geometry='))));
data_home= char(prop{2}(find(strcmp(prop{1},'data_path='))));
handles.packsize= char(prop{2}(find(strcmp(prop{1},'packsize='))));  
handles.packsize= str2double(handles.packsize);
handles.datapath= [handles.currentpath, data_home, filesep];
fclose(fid);

if strcmp(handles.scandata,'ascii') || strcmp(handles.scandata,'binary')
    if ~(exist([handles.datapath,handles.detector],'dir')==7),
        fprintf('[Error] Please use mgsgui to create the *.mat files for %s\n', ...
            handles.detector);
        return
    end
else
    fprintf('[Error] Incorrect data_format: %s\n', [handles.datapath,handles.detector]); 
    return;
end

%-------------------------------------------------------------------------------
%- read data file
%-------------------------------------------------------------------------------
fid= fopen([handles.currentpath, file_to_process],'r');
if (fid==-1) 
    fprintf('[Error] Can''t open file: %s\n', [handles.currentpath, file_to_process]); 
    return; 
end
data= fscanf(fid,'%c',5);
if strcmp(data,'x y z'),
    handles.scancoord= 'cartesian';
    fprintf('[Scan] Points are in cartesian coordinates...\n')
elseif strcmp(data,'r t z'),
    handles.scancoord= 'cylindrical';
    fprintf('[Scan] Points are in cylindrical coordinates...\n')
end
IPCoord= zeros(1,3, 'single');
while ~isempty(data)
    data= fscanf(fid,'%g %g %g',[1,3]);
    IPCoord= [IPCoord; data];
end
IPCoord(1,:)= [];
fclose(fid);

scan(handles, IPCoord, 'batch');














% update path
addpath([cd]),
% mgs
mgspath= [cd,filesep,'mgs', filesep];
addpath(mgspath),
addpath([mgspath, 'gui']),
addpath([mgspath, 'templates']),
addpath([mgspath, 'potentialmapping']),
addpath([mgspath, 'electricfield']),
addpath([mgspath, 'carriervelocity']),
addpath([mgspath, 'weigthingpotential']),
addpath([mgspath, 'scan']),
% addpath([mgspath, 'external']),
% addpath([mgspath, 'external', filesep, 'sliceomatic']),

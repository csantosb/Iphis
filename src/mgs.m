function varargout = mgs(varargin)
% MGS M-file for mgs.fig
%      MGS, by itself, creates a new MGS or raises the existing
%      singleton*.
%
%      H = MGS returns the handle to a new MGS or the handle to
%      the existing singleton*.
%
%      MGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MGS.M with the given input arguments.
%
%      MGS('Property','Value',...) creates a new MGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mgs_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mgs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mgs

% Last Modified by GUIDE v2.5 20-Feb-2006 09:31:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mgs_OpeningFcn, ...
                   'gui_OutputFcn',  @mgs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mgs is made visible.
function mgs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mgs (see VARARGIN)

%-------------------------------------------------------------------------------
%- pragmas (for compilation purpose)
%-------------------------------------------------------------------------------
%#function detectorlist_Callback
%#function go_creategeometry_Callback
%#function plot_creategeometry
%#function go_potmapping
%#function plot_potmapping
%#function go_electricfield
%#function plot_electricfield
%#function go_carriervelocity
%#function plot_carriervelocity
%#function go_weightingpotential
%#function plot_weightingpotential

%#function detectorset
%#function scanset
%#function scansingle
%#function scangui
%#function seepoint

%#function agata__yellow
%#function agata__blue
%#function agata__green
%#function agata__red
%#function planar
%#function smartpet

%-------------------------------------------------------------------------------
%- handles (contains global information)
%-------------------------------------------------------------------------------
handles.currentpath= [cd, filesep];
handles.datapath= [handles.currentpath, 'data', filesep];
handles.parameterpath= [handles.currentpath, 'parameters', filesep];
handles.version= 'MGS v5r02 IPHC/IN2P3';
handles.releasedate= ' April 07';
handles.authors= {'P. Medina', 'C. Parisel','C. Santos'};
handles.email= 'mgsteam@ires.in2p3.fr';
handles.detector= '-';              % name of the selected detector
handles.scancoord= 'cartesian';     % 1 if cartesian coordinates, 0 if cylindrical
handles.precision= 'double';        % default
handles.detectortype= 'planar';     % or 'coaxial'
handles.steptime= 1e-9;             % step time in ns (should not be changed)
handles.plotenable= 0;              % plot computation results (0= no)
handles.packsize= 30;               % compute 30 pulses at a time
handles.scanmode= 'charge';         % only charge is computed
handles.scandata= 'ascii';
% Choose default command line output for mgs
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Create directories where to store data
if ~(exist(handles.datapath, 'dir')==7),
    eval(['!mkdir data']),
end


% UIWAIT makes mgs wait for user response (see UIRESUME)
% uiwait(handles.mgsmain);


% --- Outputs from this function are returned to the command line.
function varargout = mgs_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function detectorlist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to detectorlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', readDetectorList());

%===============================================================================
function list= readDetectorList()
%readDetectorList
%   read the file mgs_detectorlist.txt containing all the detectors in use

separator= '-------';
filename= ['parameters', filesep, 'mgs_detectorlist.txt'];
fid= fopen(filename, 'rt');
if (fid==-1) fprintf('[Error] Can''t open file: %s\n', filename);
    list= {'-------'};
    return;
end
list= textread(filename, '%s', 'commentstyle', 'c++');
% replace - by separator and remove empty lines
list(find(strcmp(list,'-')))= {separator};
fclose(fid);

% --- Executes during object creation, after setting all properties.
function scanx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function scany_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function scanz_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function scanpacksize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'Value', 3);

